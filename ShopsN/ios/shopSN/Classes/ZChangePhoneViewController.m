//
//  ZChangePhoneViewController.m
//  shopSN
//
//  Created by imac on 16/7/7.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZChangePhoneViewController.h"
#import "ZYSLoginViewController.h"

@interface ZChangePhoneViewController ()<UITextFieldDelegate>

@property (strong,nonatomic) UIButton *codeBtn;

@property (strong,nonatomic) UITextField *phoneTF;

@property (strong,nonatomic) UITextField *codeTF;

/**验证码*/
@property (nonatomic,strong) NSString *identifyingCode;

@end

@implementation ZChangePhoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = __AccountBGColor;
    //self.titleLb.text = @"修改手机号";
    self.title = @"修改手机";
    
    [self initView];
}

-(void)initView{
    //mainView
    //UIView *mainView = [[UIView alloc] initWithFrame:CGRectMake(0, 64+8, __kWidth, 88)];
    UIView *mainView = [[UIView alloc] initWithFrame:CGRectMake(0, 64+10, __kWidth, 88)];
    mainView.backgroundColor = HEXCOLOR(0xffffff);
    [self.view addSubview:mainView];
    
    //1 subView
    for (int i=0; i<2; i++) {
        //(图片 文本框 底线 验证码按钮)
        UIView *subView = [[UIView alloc] initWithFrame:CGRectMake(10, 45*i, __kWidth-20, 43)];
        [mainView addSubview:subView];
        
        //图片
//        UIImageView *titleIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 11, 22, 22)];
//        titleIV.layer.masksToBounds = YES;
//        titleIV.clipsToBounds=YES;
//        titleIV.contentMode =UIViewContentModeCenter;
        UIImageView *titleIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 14, 15, 15)];
        [subView addSubview:titleIV];
        titleIV.contentMode = UIViewContentModeScaleAspectFit;
        
        //文本框
        UITextField *textField = [[UITextField alloc] initWithFrame:CGRectZero];
        [subView addSubview:textField];
//        textField.backgroundColor = __TestOColor;
        textField.font = MFont(13);
        textField.textColor = HEXCOLOR(0x333333);
        
        textField.delegate = self;
        if (i == 0) {
            //验证码 按钮
            _codeBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectW(subView)-90, 8, 90, 28)];//添加余量6
            _codeBtn.layer.borderWidth = 1;
            _codeBtn.layer.borderColor = __DefaultColor.CGColor;
            _codeBtn.layer.cornerRadius =13;
            _codeBtn.titleLabel.font   = MFont(14);
            [_codeBtn setTitle:@"获取验证码" forState:BtnNormal];
            [_codeBtn setTitleColor:__DefaultColor forState:BtnNormal];
            [_codeBtn addTarget:self action:@selector(codeBtnAction) forControlEvents:BtnTouchUpInside];
            [subView addSubview:_codeBtn];
            //输入手机号 文本框
            textField.frame = CGRectMake(CGRectXW(titleIV)+10, 0, CGRectX(_codeBtn)-CGRectXW(titleIV)-20, 44);
            _phoneTF = textField;
            _phoneTF.placeholder = @"请输入新手机号";
            //titleIV.frame = CGRectMake(2, 11, 18, 22);
            titleIV.image = [UIImage imageNamed:@"zc_sj"];
        }
        if (i == 1) {
            //输入验证码 文本框
            textField.frame = CGRectMake(CGRectXW(titleIV)+10, 0, CGRectW(subView)-CGRectXW(titleIV)-10, 44);
            _codeTF = textField;
            _codeTF.placeholder = @"请输入短信验证码";
            //_codeTF.backgroundColor = __TestGColor;
            //titleIV.frame = CGRectMake(0, 13, 22, 18);
            titleIV.image = [UIImage imageNamed:@"dx"];
        }
    }
    //2 中间line
//    for (int i=1; i<2; i++) {
//        UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(48, 44*i, __kWidth-48, 1)];
//        lineIV.backgroundColor = HEXCOLOR(0xd8d8d8);
//        [mainView addSubview:lineIV];
//    }
    
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(35, 44, CGRectW(mainView)-35, 1)];
    lineIV.backgroundColor = HEXCOLOR(0xd8d8d8);
    [mainView addSubview:lineIV];
    
    UIButton *sureBtn = [[UIButton alloc]initWithFrame:CGRectMake(15, CGRectYH(mainView)+15, __kWidth-30, 40)];
    [self.view addSubview:sureBtn];
    sureBtn.layer.cornerRadius = 4.0f;
    sureBtn.backgroundColor = __DefaultColor;
    sureBtn.titleLabel.font = MFont(15);
    [sureBtn setTitle:@"验证" forState:BtnNormal];
    [sureBtn setTitleColor:[UIColor whiteColor] forState:BtnNormal];
    [sureBtn addTarget:self action:@selector(sureAction) forControlEvents:BtnTouchUpInside];
    

}


/**
 *  获取验证码
 */
-(void)codeBtnAction{
    NSLog(@"获取验证码");
    
    NSDictionary *params =@{@"mobile":_phoneTF.text};
    
    [ZHttpRequestService POST:@"Login/sendCode" withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            
            NSLog(@"xiugai--%@",jsonDic);
            _identifyingCode = jsonDic[@"data"][0][@"sms_code"];
            
        }else{
            NSString *message = jsonDic[@"message"];
            
            [SXLoadingView showAlertHUD:message duration:2];
        }
    } failure:^(NSError *error) {
        
        [SXLoadingView showAlertHUD:@"失败" duration:SXLoadingTime];
        
    } animated:true withView:nil];
    
    
}
/**
 *  验证
 */
-(void)sureAction{
    NSLog(@"验证");
    
    if (_identifyingCode&&_identifyingCode.length!=0) {
        if ([_identifyingCode isEqualToString:_codeTF.text]) {
            NSDictionary *params =@{@"id":[UdStorage getObjectforKey:Userid],
                                    @"mobile":_phoneTF.text};
            [ZHttpRequestService POST:@"change_phone" withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
                if (succe) {

                    NSString *message = jsonDic[@"message"];
                    [SXLoadingView showAlertHUD:message duration:2];

                    NSLog(@"xiugai--%@",jsonDic);
                }else{
                    NSString *message = jsonDic[@"message"];
                    
                    [SXLoadingView showAlertHUD:message duration:2];
                }
                
                
            } failure:^(NSError *error) {
                
                [SXLoadingView showAlertHUD:@"失败" duration:SXLoadingTime];
                
            } animated:true withView:nil];
        }else{
            [SXLoadingView showAlertHUD:@"验证码错误" duration:1];
        }
        
    }else{
         [SXLoadingView showAlertHUD:@"验证码为空" duration:1];
        return;
    }
    
}

#pragma mark -UITextFiledDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
