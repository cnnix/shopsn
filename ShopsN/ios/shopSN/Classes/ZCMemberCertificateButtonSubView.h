//
//  ZCMemberCertificateButtonSubView.h
//  shopSN
//
//  Created by yisu on 16/7/15.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 我的模块 会员资格 顶部视图
 *
 *  按钮 视图
 *
 */
#import <UIKit/UIKit.h>

@interface ZCMemberCertificateButtonSubView : UIView

/** 等级 V label*/
@property (nonatomic, strong) UILabel *cs_certificateVLb;


/** 等级 标题*/
@property (nonatomic, strong) UILabel *cs_titleLb;


/** 等级 描述*/
@property (nonatomic, strong) UILabel *cs_detailLb;


/** 等级 购买图片*/
@property (nonatomic, strong) UIImageView *cs_buyIV;

@end
