//
//  ZCMemberCertificateButtonSubView.m
//  shopSN
//
//  Created by yisu on 16/7/15.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCMemberCertificateButtonSubView.h"

@implementation ZCMemberCertificateButtonSubView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        
        [self initSubViews];
        
    }
    return self;
}


- (void)initSubViews {
    //背景
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [self addSubview:bgView];
    
    //1 等级V label
    //边线 color(0 108 41)  (0x006c29)
    //背景 color(80 177 68)  (0x50b144)
    _cs_certificateVLb = [[UILabel alloc] initWithFrame:CGRectMake((CGRectW(bgView)-30)/2, 45, 30, 30)];
    [bgView addSubview:_cs_certificateVLb];
    _cs_certificateVLb.backgroundColor = HEXCOLOR(0x50b144);
    _cs_certificateVLb.layer.borderWidth = 2.0f;
    _cs_certificateVLb.layer.borderColor = HEXCOLOR(0x006c29).CGColor;
    _cs_certificateVLb.layer.cornerRadius = 15.0f;
    _cs_certificateVLb.font = MFont(12);
    _cs_certificateVLb.textColor = HEXCOLOR(0x006c29);
    _cs_certificateVLb.textAlignment = NSTextAlignmentCenter;
//    _cs_certificateVLb.text = @"V1";
    
    //2 标题 label
    _cs_titleLb = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectYH(_cs_certificateVLb)+5, CGRectW(bgView), 15)];
    [bgView addSubview:_cs_titleLb];
    _cs_titleLb.font = MFont(11);
    _cs_titleLb.textColor = HEXCOLOR(0x006c29);
    _cs_titleLb.textAlignment = NSTextAlignmentCenter;
    
    //选中颜色 color(254 244 18)  (0xfef412)
    //3 购买图片
    _cs_buyIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectH(bgView)-30, 20, 20)];
    [bgView addSubview:_cs_buyIV];
    _cs_buyIV.backgroundColor = HEXCOLOR(0xfef412);//等待图片更新
    
    
    //4 描述label
    _cs_detailLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_cs_buyIV)+10, CGRectYH(_cs_titleLb)+5, CGRectW(bgView)-CGRectW(_cs_buyIV)-20, 15)];
    [bgView addSubview:_cs_detailLb];
//    _cs_detailLb.backgroundColor = __TestOColor;
    _cs_detailLb.font = MFont(11);
    _cs_detailLb.textColor = HEXCOLOR(0x006c29);
    
}

//设置label指定位置字体 大小
//- (void)setLabeltextAttributes:(NSString *)text {
//    NSInteger length = text.length;
//     NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:text];
//    [attri addAttribute:NSFontAttributeName value:MFont(9) range:NSMakeRange(length-3, 3)];
//    [_cs_detailLb setAttributedText:attri];
//}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
