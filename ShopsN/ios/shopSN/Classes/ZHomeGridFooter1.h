//
//  ZHomeGridFooter1.h
//  shopSN
//
//  Created by yisu on 16/8/11.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 首页页面 collectionView
 *
 *  会员商品分类 footer
 *
 */
#import <UIKit/UIKit.h>

@interface ZHomeGridFooter1 : UICollectionReusableView

@end
