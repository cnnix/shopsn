//
//  ZGoodsCell.m
//  亿速
//
//  Created by yisu on 16/6/27.
//  Copyright © 2016年 wshan. All rights reserved.
//

#import "ZGoodsCell.h"

@implementation ZGoodsCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        bgView.backgroundColor = HEXCOLOR(0xffffff);
        [self.contentView addSubview:bgView];
        
        [self initSubView:bgView];
    }
    return self;
}

- (void)initSubView:(UIView *)bgView {
    //商品图片
    _gl_goodsIV = [[UIImageView alloc] initWithFrame:CGRectMake(Cell_MINISPACE, Cell_SPACE, CGRectW(bgView)-10, 150)];
    _gl_goodsIV.image = MImage(@"pic11");
    [bgView addSubview:_gl_goodsIV];
    
    //商品描述 ***考虑文字内容可能会很多 空间预留是三行
    _gl_descLb = [[UILabel alloc] initWithFrame:CGRectMake(Cell_SPACE, CGRectYH(_gl_goodsIV)+Cell_MINISPACE, CGRectW(bgView)-20, 60)];
    //_gl_descLb.backgroundColor = __TestOColor;
    _gl_descLb.font = MFont(11);
    _gl_descLb.textColor = HEXCOLOR(0x333333);
    _gl_descLb.numberOfLines = 0;
    [bgView addSubview:_gl_descLb];
    
    
    //商品价格 color (255 47 0) (ff2f00)
    _gl_pricesLb = [[UILabel alloc] initWithFrame:CGRectMake(Cell_SPACE, CGRectYH(_gl_descLb)+Cell_SPACE, 80, 20)];
    //_gl_pricesLb.backgroundColor = __TestGColor;
    _gl_pricesLb.font = MFont(13);
    _gl_pricesLb.textColor = HEXCOLOR(0xff2f00);
    [bgView addSubview:_gl_pricesLb];
    
    
//    //商品销量 单位等 (件已售)
//    UILabel *salesUnitLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(bgView)-30-Cell_MINISPACE, CGRectYH(_gl_descLb)+Cell_SPACE, 30, 20)];
//    //salesUnitLb.backgroundColor = __TestOColor;
//    salesUnitLb.font = MFont(10);
//    salesUnitLb.textColor = HEXCOLOR(0x999999);
//    salesUnitLb.text = @"件已售";
//    [bgView addSubview:salesUnitLb];
    
    
    //商品销量 数量 *** 预留了一定空间
    _gl_salesLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(bgView)-60-10, CGRectYH(_gl_descLb)+Cell_SPACE, 60, 20)];
    //_gl_salesLb.backgroundColor = __TestOColor;
    _gl_salesLb.font = MFont(10);
    _gl_salesLb.textColor = HEXCOLOR(0x999999);
    _gl_salesLb.textAlignment = NSTextAlignmentRight;
    [bgView addSubview:_gl_salesLb];
    
    //内容测试
    _gl_descLb.text  = @"日本原装Merries花王 纸尿裤L54(9-14KG)【4包装】";
    _gl_pricesLb.text = @"￥472";
    _gl_salesLb.text = @"1.8万件已售";
    
    
}


@end
