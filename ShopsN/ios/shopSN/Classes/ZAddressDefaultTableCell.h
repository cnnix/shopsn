//
//  ZDefaultTableCell.h
//  shopSN
//
//  Created by imac on 16/7/8.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "BaseTableViewCell.h"

@protocol ZAddressDefaultTableCelldelegate <NSObject>

-(void)edit:(UIButton*)sender;

-(void)delAddress:(UIButton*)sender;

@end

@interface ZAddressDefaultTableCell : BaseTableViewCell
/**
 *  收货人姓名
 */
@property (strong,nonatomic) UILabel *nameLb;
/**
 *  地址
 */
@property (strong,nonatomic) UILabel *addressLb;
/**
 *  收货人手机
 */
@property (strong,nonatomic) UILabel *mobileLb;
/**
 *  编辑按钮
 */
@property (strong,nonatomic) UIButton *editBtn;
/**
 *  删除按钮
 */
@property (strong,nonatomic) UIButton *delBtn;

@property (weak,nonatomic) id<ZAddressDefaultTableCelldelegate>delegate;

@end
