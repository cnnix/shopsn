//
//  ZCenterPersonalInfoCell.h
//  shopSN
//
//  Created by chang on 16/7/17.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 我的模块 个人信息子页面
 *
 *  修改性别页面 cell
 *
 */
#import "BaseTableViewCell.h"

@interface ZCenterPersonalInfoCell : BaseTableViewCell

@property(nonatomic, strong)UIImageView *titleIV;
@property(nonatomic, strong)UILabel *titleLb;
@property(nonatomic, strong)UIImageView *lineIV;


@end
