//
//  ZCategorySubHeader.h
//  亿速
//
//  Created by chang on 16/6/24.
//  Copyright © 2016年 wshan. All rights reserved.
//

/* 提供 子分类控制器 collectionView
 *
 *  header视图
 *
 */
#import <UIKit/UIKit.h>

@interface ZCategorySubHeader : UICollectionReusableView

/** 标题 */
@property (nonatomic, strong) UILabel *titleLb;


@end
