//
//  ZCenterMessageViewController.m
//  shopSN
//
//  Created by chang on 16/7/8.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCenterMessageViewController.h"
#import "ZMessageNewCell.h"

//#import "ZCenterMessageListViewController.h"
//
//#import "ZMessageCell.h"

@interface ZCenterMessageViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    UITableView *_tableView;
}

@end

@implementation ZCenterMessageViewController

#pragma mark - ==== 页面设置 =====
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = [NSString stringWithFormat:@"消息(%ld)",(unsigned long)self.messageArray.count];
    
    //视图 初始化
    [self initViews];
    
}


//视图 初始化
- (void)initViews {
    
    //顶部边线
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 64, __kWidth, 1)];
    [self.view addSubview:lineIV];
    lineIV.backgroundColor = HEXCOLOR(0xdedede);
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 65, __kWidth, __kHeight-65)];
    [self.view addSubview:_tableView];
//    _tableView.backgroundColor = __TestOColor;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.dataSource = self;
    _tableView.delegate   = self;
    
  
    
}


#pragma mark- ===== tableView DataSource and Delegate =====
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.messageArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZMessageNewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageCell"];
    if (!cell) {
        cell = [[ZMessageNewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MessageCell"];
    }
    cell.flagIV.hidden = YES;
    NSString *sendStr = self.messageArray[indexPath.row][@"sendto"];
    if ([sendStr isEqualToString:@"0"]) {
        cell.titleLb.text = @"系统消息";
    }else{
        cell.titleLb.text = @"用户消息";
    }
    cell.timeLb.text   = self.messageArray[indexPath.row][@"create_time"];
    cell.detailLb.text = self.messageArray[indexPath.row][@"news_info"];
    cell.detailLb.numberOfLines = 0;
//    CGSize boundSize = CGSizeMake(216, CGFLOAT_MAX);
//    cell.userInteractionEnabled = NO;
//    cell.detailLb.numberOfLines = 0;
//    CGSize requiredSize = [cell.detailLb.text sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:boundSize lineBreakMode:UILineBreakModeCharacterWrap];
//    CGRect rect = cell.frame;
//    rect.size.height = requiredSize.height+5;
//    cell.frame = rect;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 110;
//    ZMessageNewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
//    UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
//    
//    NSLog(@"cell height %f",cell.frame.size.height);
//    return cell.frame.size.height+20;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}


#pragma mark - ==== others =====
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
