//
//  ZHomeGirdGoodsFooter2.m
//  shopSN
//
//  Created by yisu on 16/10/14.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZHomeGirdGoodsFooter2.h"

@interface ZHomeGirdGoodsFooter2 ()
{
    BaseView *bgView;
    
//    NSArray *textArray;
    
}

@property (nonatomic, strong) YFRollingLabel *footerLb;


@end

@implementation ZHomeGirdGoodsFooter2

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        
        bgView = [[BaseView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        bgView.backgroundColor = HEXCOLOR(0xf0f0f0);
        [self addSubview:bgView];
        
        //[self getData];
        
        [self initSubViews:bgView];
        
    }
    return self;
}

- (void)getData {
    [ZHttpRequestService GETHome:@"gonggao" withParameters:nil success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            
            NSString *str = jsonDic[@"data"];
            NSLog(@"%@",str);
            [self addlabelScrolllWithView:bgView andMessage:str];
        }
    } failure:^(NSError *error) {
        //
    } animated:NO withView:nil];
    
}

- (void)initSubViews:(UIView *)view {
    
    
    
    //上边
//    UIImageView *lineTopIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 40, CGRectW(bgView), 10)];
//    [view addSubview:lineTopIV];
//    lineTopIV.backgroundColor = HEXCOLOR(0xffffff);
    
    
    
    //图片
     _commonGoodsFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 40, CGRectW(bgView), CGRectH(bgView)-20)];
    [view addSubview:_commonGoodsFooterView];
    //_commonGoodsFooterView.backgroundColor = __TestOColor;
    
    _commonGoodsFooterCenterIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(_commonGoodsFooterView), CGRectH(_commonGoodsFooterView))];
    [_commonGoodsFooterView addSubview:_commonGoodsFooterCenterIV];
    //_commonGoodsFooterCenterIV.backgroundColor = __TestGColor;
    
    
    //底线
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectH(bgView)-1, CGRectW(bgView), 1)];
    [view addSubview:lineIV];
    lineIV.backgroundColor = HEXCOLOR(0xdedede);
    
}

- (void)addlabelScrolllWithView:(UIView *)view andMessage:(NSString *)message {
    //滚动文字
    
    NSArray *textArray = @[message];
    _scrollView = [[UIScrollView  alloc] initWithFrame:CGRectMake(0, 0, CGRectW(bgView), 40)];
    [bgView addSubview:_scrollView];
    _scrollView.backgroundColor = [UIColor lightGrayColor];
    _scrollView.contentSize = CGSizeMake(self.frame.size.width, 1000);
    
    _footerLb = [[YFRollingLabel alloc] initWithFrame:CGRectMake(5, 0, CGRectW(bgView)-10, 40) textArray:textArray font:MFont(13) textColor:[UIColor redColor]];
    [_scrollView addSubview:_footerLb];
    //_footerLb.backgroundColor = [UIColor orangeColor];
    _footerLb.speed = 1;
    [_footerLb setOrientation:RollingOrientationLeft];
    [_footerLb setInternalWidth:_footerLb.frame.size.width / 3];
    
    //Label Click Event Using Block
    _footerLb.labelClickBlock = ^(NSInteger index){
        NSString *text = [textArray objectAtIndex:index];
        NSLog(@"You Tapped item:%li , and the text is %@",(long)index,text);
    };
}


@end
