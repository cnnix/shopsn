//
//  ZMyZZUCell.h
//  shopSN
//
//  Created by yisu on 16/10/8.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 个人中心模块
 *
 *  我的掌中 cell
 *
 */
#import "BaseTableViewCell.h"

@interface ZMyZZUCell : BaseTableViewCell

@property(nonatomic, strong)UIImageView *titleIV;
@property(nonatomic, strong)UILabel *titleLb;
@property(nonatomic, strong)UIImageView *lineIV;

@end
