//
//  ZGoodsListSetionView.h
//  shopSN
//
//  Created by chang on 16/6/26.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 商品列表页面
 *
 *  功能选择视图
 *
 */

#import "BaseView.h"

@class ZGoodsListSetionView;
@protocol ZGoodsListSetionViewDelegate <NSObject>


- (void)showSiftView:(id)sender;

@end

@interface ZGoodsListSetionView : BaseView

@property (nonatomic, weak) id<ZGoodsListSetionViewDelegate>delegate;


@end
