//
//  ZCenterMyQRCodeViewController.h
//  shopSN
//
//  Created by chang on 16/7/9.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 分享shopSN 页面
 *
 *  我的二维码名片 视图控制器
 *
 */
#import "BaseViewController.h"

@interface ZCenterMyQRCodeViewController : BaseViewController

/** 生成二维码 字符串内容 */
@property (nonatomic, copy) NSString *qrCodeStr;

/** 用户昵称 */
@property (nonatomic, copy) NSString *nickNameStr;
/**二维码*/
@property (nonatomic,strong) UIImage *myImage;
@property (nonatomic, strong)UIImageView *qrCodeView;


@end
