//
//  ZCommonGoodsInfoViewController.h
//  shopSN
//
//  Created by yisu on 16/8/2.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 普通商品信息页面
 *
 *  主视图控制器
 *
 */
#import "ZBaseViewController.h"



@interface ZCommonGoodsInfoViewController : ZBaseViewController


/** 商品信息页面 商品id */
@property (copy, nonatomic) NSString *comGoodsID;

@end
