//
//  ZCommonWebViewController.m
//  shopSN
//
//  Created by yisu on 16/10/9.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCommonWebViewController.h"

#import "ZYSLoginViewController.h"//亿速登录

@interface ZCommonWebViewController ()<UIWebViewDelegate>
{
    UIView *bgView;
    UIWebView *webView;
    UIActivityIndicatorView *activityIndicator;
}
@end

@implementation ZCommonWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.titleStr;
    
    //bgView
    bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(self.view), CGRectH(self.view))];
    bgView.backgroundColor = HEXCOLOR(0xffffff);
    [self.view addSubview:bgView];
    
    //lineIV
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(bgView), 1)];
    lineIV.backgroundColor = HEXCOLOR(0xdedede);
    [bgView addSubview:lineIV];
    
    //webView
    webView = [[UIWebView alloc] initWithFrame:CGRectMake(5, 1, CGRectW(bgView)-10, CGRectH(bgView)-1)];
    [bgView addSubview:webView];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.urlStr]];
    [webView loadRequest:request];
    webView.delegate = self;
}

#pragma mark - 自定义导航栏
- (void)addNavigationBar {
    //返回按钮
    UIButton *backBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 22, 22)];
    [backBtn setBackgroundImage:MImage(@"fanhui.png") forState:BtnNormal];
    [backBtn addTarget:self action:@selector(back) forControlEvents:BtnTouchUpInside];
    
    UIBarButtonItem *backItem=[[UIBarButtonItem alloc]initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = backItem;
    
    
    //右侧按钮
    UIButton *rightBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 22)];
    //rightBtn.backgroundColor = __DefaultColor;
    rightBtn.titleLabel.font = MFont(14);
    [rightBtn setTitle:@"重新登录" forState:BtnNormal];
    [rightBtn setTitleColor:__DefaultColor forState:BtnNormal];
    
    [rightBtn addTarget:self action:@selector(rightButtonAciton) forControlEvents:BtnTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
}

/**
 *  重新登录  调用
 */
-(void)rightButtonAciton{
    NSLog(@"重新登录");
    
    //清除记录
    NSString *nilStr = nil;
    [UdStorage storageObject:nilStr forKey:Userid];
    [UdStorage storageObject:nilStr forKey:UserType];
    [UdStorage storageObject:nilStr forKey:UserPhone];
    
    
    
    //重新登录
//    ZLoginViewController *vc = [[ZLoginViewController alloc] init];
    ZYSLoginViewController *vc = [[ZYSLoginViewController alloc]init];
    
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - ==== webView Delegate =====
- (void) webViewDidStartLoad:(UIWebView *)webView
{
    NSLog(@"webViewDidStartLoad");
    
    //创建UIActivityIndicatorView背底半透明View
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, __kHeight)];
    [view setTag:409];
    [view setBackgroundColor:[UIColor blackColor]];
    [view setAlpha:0.5];
    [bgView addSubview:view];
    
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)];
    [activityIndicator setCenter:view.center];
    [activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    [view addSubview:activityIndicator];
    
    [activityIndicator startAnimating];
    
}
- (void) webViewDidFinishLoad:(UIWebView *)webView
{
    [activityIndicator stopAnimating];
    UIView *view = (UIView*)[self.view viewWithTag:409];
    [view removeFromSuperview];
    NSLog(@"webViewDidFinishLoad");
}
- (void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"didFailLoadWithError:%@", error);
    [activityIndicator stopAnimating];
    UIView *view = (UIView*)[self.view viewWithTag:409];
    [view removeFromSuperview];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
