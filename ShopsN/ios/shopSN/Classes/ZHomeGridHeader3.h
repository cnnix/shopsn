//
//  ZHomeGridHeader3.h
//  shopSN
//
//  Created by yisu on 16/8/19.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 首页页面 collectionView
 *
 *  中间 推荐内容 header
 *
 */
#import <UIKit/UIKit.h>

@protocol ZHomeGridHeader3Delegate <NSObject>

- (void)lookmore:(UIButton *)sender;

@end

@interface ZHomeGridHeader3 : UICollectionReusableView

/** 推荐内容 header 标题 */
@property (nonatomic, strong) UILabel *recommendTitleLb;

/**
 *  更多按钮
 */
@property (strong,nonatomic) UIButton *moreBtn;

@property (weak,nonatomic) id<ZHomeGridHeader3Delegate>delegate;

@end
