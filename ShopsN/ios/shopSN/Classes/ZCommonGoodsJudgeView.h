//
//  ZCommonGoodsJudgeView.h
//  shopSN
//
//  Created by yisu on 16/8/3.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 普通商品 
 *
 *   评价 view
 *
 */
#import "BaseView.h"
#import "ZBigStarView.h"

//@class ZCommonGoodsJudgeView;
//@protocol ZCommonGoodsJudgeDelegate <NSObject>
//
//- (void)commonGoodsJudgeBottomBtnAction:(UIButton *)sender;
//
//@end

@interface ZCommonGoodsJudgeView : BaseView

///** 普通商品评价页面 代理方法 */
//@property (weak, nonatomic) id<ZCommonGoodsJudgeDelegate>delegate;

/** 普通商品评价页面 加载评价数组 */
- (void)loadCommonJudgeData:(NSArray *)array;

@end
