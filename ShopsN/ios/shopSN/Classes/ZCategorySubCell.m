//
//  ZCategorySubCell.m
//  亿速
//
//  Created by chang on 16/6/24.
//  Copyright © 2016年 wshan. All rights reserved.
//

#import "ZCategorySubCell.h"

@implementation ZCategorySubCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        //bgView.backgroundColor = HEXCOLOR(0xffffff);
        //bgView.backgroundColor = [UIColor purpleColor];
        [self.contentView addSubview:bgView];
        
        [self initSubViews:bgView];
        
        
        
    }
    return self;
}

- (void)initSubViews:(UIView *)bgView {
    
//原设计方案
    //goodsIV
//    _goodsIV = [[UIImageView alloc] initWithFrame:CGRectMake(Cell_MINISPACE, Cell_MINISPACE, CGRectW(bgView)-10, 70)];
//    _goodsIV.image = MImage(@"pic10");
//    [bgView addSubview:_goodsIV];
    
    //goodsLb
//    _goodsLb = [[UILabel alloc] initWithFrame:CGRectMake((CGRectW(bgView)-60)/2, CGRectYH(_goodsIV), 60, 20)];
//    //_goodsLb.backgroundColor = __TestGColor;
//    _goodsLb.font = MFont(10);
//    _goodsLb.textColor = HEXCOLOR(0x333333);
//    _goodsLb.textAlignment = NSTextAlignmentCenter;
//    _goodsLb.text = @"哈衣爬服";
//    [bgView addSubview:_goodsLb];
    
    
    
//Web设计方案
    //goodsLb
    _goodsLb = [[UILabel alloc] initWithFrame:CGRectMake((CGRectW(bgView)-80)/2, (CGRectH(bgView)-20)/2, 80, 20)];
    //_goodsLb.backgroundColor = __TestGColor;
    _goodsLb.font = MFont(14);
    _goodsLb.textColor = __DefaultColor;
    _goodsLb.textAlignment = NSTextAlignmentCenter;
    _goodsLb.text = @"哈衣爬服";
    [bgView addSubview:_goodsLb];
    
    //lineIV
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(5, CGRectH(bgView)-2, CGRectW(bgView)-10, 2)];
    [bgView addSubview:lineIV];
    lineIV.backgroundColor = __DefaultColor;
    
}





@end
