//
//  ZPayOrderInfoCell.h
//  shopSN
//
//  Created by chang on 16/7/3.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 结算页面
 *
 *  订单信息 Cell
 *
 */
#import <UIKit/UIKit.h>

@interface ZPayOrderInfoCell : UITableViewCell

/** 商品图片 */
@property (nonatomic, copy) NSString *goodsImageName;

/** 商品描述 */
@property (nonatomic, copy) NSString *goodsDetail;

/** 商品价格 */
@property (nonatomic, copy) NSString *goodsPrice;

/** 商品数量 */
@property (nonatomic, copy) NSString *goodsNum;

/** 商品合计价格 */
@property (nonatomic, copy) NSString *goodsSettlePrice;



/**图片*/
@property (nonatomic,strong) UIImageView *cellImage;
/**描述*/
@property (nonatomic,strong) UILabel *cellDetail;
/**商品价格*/
@property (nonatomic,strong) UILabel *cellPrice;
/**商品数量*/
@property (nonatomic,strong) UILabel *cellNumber;
/**商品合计价格*/
@property (nonatomic,strong) UILabel *cellAllPrice;

/**套餐名*/
@property (nonatomic,strong) UILabel *cellSetName;
/**送积分*/
@property (nonatomic,strong) UILabel *cellRetuPoint;
/**id*/
@property (nonatomic,copy) NSString *cellCartId;




@end
