//
//  ZCMyOrderWaitingPayFooter.h
//  shopSN
//
//  Created by yisu on 16/7/14.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 我的模块 我的订单页面
 *
 *  待付款 footer
 *
 */
#import "BaseView.h"

@class ZCMyOrderWaitingPayFooter;
@protocol ZCMyOrderWatingPayFooterDelegate <NSObject>

- (void)orderWaitPayOperating:(ZOrderModel *)order andButton:(UIButton *)button;

@end

@interface ZCMyOrderWaitingPayFooter : BaseView

/** 待付款订单  订单 */
@property (nonatomic, strong) ZOrderModel *order;



/** 待付款订单 商品小计价格 */
@property (nonatomic, strong) UILabel *settlePriceLb;

/** 待付款订单 商品小计数量 */
@property (nonatomic, strong) UILabel *settleCountLb;

/** 待付款订单 商品下单时间 */
@property (nonatomic, strong) UILabel *orderTimeLb;

/** 待付款订单cell 代理方法 */
@property (weak, nonatomic) id<ZCMyOrderWatingPayFooterDelegate>delegate;

/** 添加类商品信息 */
- (void)addTourismViewInfoDataWithAdultsNum:(NSString *)adultsNum andAdultsPrice:(NSString *)adultsPrice andMinorsNum:(NSString *)minorsNum andMinorsPrice:(NSString *)minorsPrice;

@end
