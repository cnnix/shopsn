//
//  ZPayTypeCell.m
//  shopSN
//
//  Created by chang on 16/7/4.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZPayTypeCell.h"

@implementation ZPayTypeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        //1 勾选框
        _pt_iconIV = [[UIButton alloc] initWithFrame:CGRectMake(10, 20, 20, 20)];
        [self addSubview:_pt_iconIV];
        [_pt_iconIV setImage:MImage(@"yuang01") forState:UIControlStateNormal];
        [_pt_iconIV setImage:MImage(@"yuang02") forState:UIControlStateSelected];
        _pt_iconIV.userInteractionEnabled = false; 
        
        //2 支付类型图片
        _pt_payIV = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectXW(_pt_iconIV)+10, 15, 30, 30)];
        [self addSubview:_pt_payIV];
        _pt_payIV.image = MImage(@"weixin");
        
        //3 标题
        _pt_titleLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_pt_payIV)+5, 5, __kWidth-20-15-CGRectW(_pt_iconIV)-CGRectW(_pt_payIV), 25)];
        [self addSubview:_pt_titleLb];
//        _pt_titleLb.backgroundColor = __TestOColor;
        _pt_titleLb.font = MFont(14);
        _pt_titleLb.textColor = HEXCOLOR(0x333333);
        _pt_titleLb.text = @"微信支付";
        
        //4 描述
        _pt_detailLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_pt_payIV)+5, CGRectYH(_pt_titleLb), __kWidth-20-15-CGRectW(_pt_iconIV)-CGRectW(_pt_payIV), 20)];
        [self addSubview:_pt_detailLb];
        //_pt_detailLb.backgroundColor = __TestGColor;
        _pt_detailLb.font = MFont(12);
        _pt_detailLb.textColor = HEXCOLOR(0x999999);
        _pt_detailLb.text = @"推荐已在微信中绑定银行卡的用户使用";
        
        
        //5 边线
        UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(10, 55, __kWidth-10, 1)];
        [self addSubview:lineIV];
        lineIV.backgroundColor = HEXCOLOR(0xdedede);
        
    }
    return self;
}








- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
