//
//  ZGoodsSiftView.m
//  shopSN
//
//  Created by yisu on 16/6/27.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZGoodsSiftView.h"

@interface ZGoodsSiftView ()
{
    UIView   *bgView;  //主视图 背景视图
    UIView   *btnView; //按钮视图
    UIView   *middleView; //中间视图
    UIView   *bottomView; //底部视图
    UIButton *g_allBtn;//类别第一个按钮
    UIButton *b_allBtn;//品牌第一个按钮
}

/** 筛选视图 商品数量 */
@property (nonatomic, copy) NSString *goodsNum;

/** 筛选视图 商品品牌 原始数组 */
@property (nonatomic, copy) NSArray *oranginalArray;//默认全部


/** 筛选视图 商品品牌 重新选则后数组 */
@property (nonatomic, strong) NSMutableArray *chooseArray;//默认全部





@end

@implementation ZGoodsSiftView

- (NSMutableArray *)chooseArray {
    if (!_chooseArray) {
        _chooseArray = [NSMutableArray array];
    }
    return _chooseArray;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        
        bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        bgView.backgroundColor = HEXCOLOR(0xffffff);
        
        [self addSubview:bgView];
        
        
        [self getData];
       
        [self initSubView:(bgView)];//加载子视图
        
        
//
//        bgView = [self createBgView];
//        [self addSubview:bgView];
        
        
    }
    return self;
}

#pragma mark - ===== 获取相关数据 ====
- (void)getData {
    
    //测试数据
    _goodsNum = @"11160";
    _gs_categoryName = @"纸尿裤";
    
    // Huggies/好奇 Pamers/帮宝适 妈咪宝贝 雀氏 奇酷 Moony 安乐尔 Merries花王
    _oranginalArray = @[@"全部",@"Huggies/好奇",@"Pamers/帮宝适",@"妈咪宝贝",@"雀氏",@"奇酷",@"Moony",@"安乐尔",@"Merries花王"];
    
    
}




#pragma mark - ===== 创建子视图 =====



//加载 主页面子视图
- (void)initSubView:(UIView *)view {
    
    //中间视图
    middleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(view), CGRectH(view)-44)];
    //middleView.backgroundColor = __TestOColor;
    [view addSubview:middleView];
    
    [self addMiddleViewSubView:middleView];//加载其 子视图
    
    
    
    //底部视图
    bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectH(view)-44, CGRectW(view), 44)];
    //bottomView.backgroundColor = __TestOColor;
    [view addSubview:bottomView];
    
    //1 商品数量 view
    UIView *numberView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(bottomView)/3, 44)];
    //numberView.backgroundColor = __TestGColor;
    numberView.layer.borderWidth = 1.0;
    numberView.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
    [bottomView addSubview:numberView];
    
    [self addNumberViewSubView:numberView];//加载其 子视图
    
    
    //2 重置按钮
    UIButton *restBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectXW(numberView), 0, CGRectW(bottomView)/3, 44)];
    restBtn.layer.borderWidth = 1.0;
    restBtn.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
    restBtn.backgroundColor = HEXCOLOR(0xf0f0f0);
    restBtn.titleLabel.font = MFont(14);
    [restBtn setTitle:@"重置" forState:BtnNormal];
    [restBtn setTitleColor:HEXCOLOR(0x333333) forState:BtnNormal];
    [restBtn addTarget:self action:@selector(siftRestBtnAction) forControlEvents:BtnTouchUpInside];
    [bottomView addSubview:restBtn];
    
    //2 完成按钮
    UIButton *sureBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectXW(restBtn), 0, CGRectW(bottomView)/3, 44)];
    sureBtn.layer.borderWidth = 1.0;
    sureBtn.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
    sureBtn.backgroundColor = __DefaultColor;
    sureBtn.titleLabel.font = MFont(14);
    [sureBtn setTitle:@"完成" forState:BtnNormal];
    [sureBtn setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
    [sureBtn addTarget:self action:@selector(siftSureBtnAction) forControlEvents:BtnTouchUpInside];
    [bottomView addSubview:sureBtn];
}

//加载中间视图 子视图
- (void)addMiddleViewSubView:(UIView *)view {
    
    //分类相关
    //1 label 分类
    UILabel *g_lb = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, CGRectW(view)-20, 20)];
    g_lb.font = MFont(14);
    g_lb.textColor = HEXCOLOR(0x333333);
    g_lb.text = @"类别";
    [middleView addSubview:g_lb];
    
    //2 button  全部  传递值内容
    g_allBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, CGRectYH(g_lb)+10, (CGRectW(view)-20-15*2)/3, 36)];
    g_allBtn.layer.borderWidth = 1.0;
    g_allBtn.layer.borderColor = __DefaultColor.CGColor;
    g_allBtn.titleLabel.font = MFont(13);
    [g_allBtn setTitle:@"全部" forState:BtnNormal];
    [g_allBtn setTitleColor:__DefaultColor forState:BtnNormal];
    [g_allBtn setImage:MImage(@"xuanzhong") forState:BtnNormal];
    [g_allBtn setImage:MImage(@"xuanzhong_b") forState:BtnStateSelected];
    g_allBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [view addSubview:g_allBtn];
    
    _g_chooseBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectXW(g_allBtn)+15, CGRectYH(g_lb)+10, (CGRectW(view)-20-15*2)/3, 36)];
    _g_chooseBtn.layer.borderWidth = 1.0;
    _g_chooseBtn.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
    _g_chooseBtn.titleLabel.font = MFont(13);
    [_g_chooseBtn setTitle:_gs_categoryName forState:BtnNormal];
    [_g_chooseBtn setTitleColor:HEXCOLOR(0x333333) forState:BtnNormal];
    [view addSubview:_g_chooseBtn];
    
    
    
    //品牌相关
    //1 label 分类
    UILabel *b_lb = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectYH(g_allBtn)+15, CGRectW(middleView)-20, 20)];
    b_lb.font = MFont(14);
    b_lb.textColor = HEXCOLOR(0x333333);
    b_lb.text = @"品牌";
    [view addSubview:b_lb];
    
    btnView = [[UIView alloc] initWithFrame:CGRectMake(10, CGRectYH(b_lb)+10, CGRectW(view)-20, (36+15)*3)];
    //btnView.backgroundColor = __TestGColor;
    //NSLog(@"w:%lf === h:%lf",btnView.frame.size.width,btnView.frame.size.height);
    [view addSubview:btnView];
    
    
    //w:300.000000 === h:153.000000
    for (int i=0; i<_oranginalArray.count; i++) {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake((i%3)*(CGRectW(btnView)/3+5), ((i/3)*(36+15)), (CGRectW(btnView)-30)/3, 36)];
         //btn.backgroundColor = __TestOColor;
        btn.layer.borderWidth = 1.0;
        btn.titleLabel.font = MFont(13);
        btn.titleLabel.numberOfLines = 0;
        [btn setTitle:_oranginalArray[i] forState:BtnNormal];
        [btn setTitleColor:HEXCOLOR(0x333333) forState:BtnNormal];
        [btn setTitleColor:__DefaultColor forState:BtnStateSelected];
        [btn setImage:MImage(@"xuanzhong_b") forState:BtnNormal];
        [btn setImage:MImage(@"xuanzhong") forState:BtnStateSelected];
        btn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        btn.layer.masksToBounds = YES;
        btn.tag = 50 + i;
        [btn addTarget:self action:@selector(newChooseBtnAciton:) forControlEvents:BtnTouchUpInside];
        btn.selected = NO;
        if (i == 0) {
            btn.selected = YES;
            btn.layer.borderColor = __DefaultColor.CGColor;
            b_allBtn = btn;
            
        }else{
            btn.layer.borderColor = HEXCOLOR(0xdedede).CGColor;

        }
        
        
        [btnView addSubview:btn];
    }
    
    
    
}

//设置品牌按钮相关内容
//- (void)initChooseButtonWith:(CGRect)frame withIndex:(NSInteger)index withTitle:(NSString *)title {
//    
//}
//

//加载商品数量视图 子视图
- (void)addNumberViewSubView:(UIView *)numberView {
    
    //1 头label -有
    UILabel *h_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 12, 44)];
    //h_label.backgroundColor = __TestOColor;
    h_label.font = MFont(12);
    h_label.textColor = HEXCOLOR(0x333333);
    h_label.text= @"有";
    [numberView addSubview:h_label];
    
    //2 尾label -件商品
    UILabel *t_label = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(numberView)-36-10, 0, 36, 44)];
    //t_label.backgroundColor = __TestOColor;
    t_label.font = MFont(12);
    t_label.textColor = HEXCOLOR(0x333333);
    t_label.text= @"件商品";
    [numberView addSubview:t_label];
    
    //3 中间 商品数量label
    UILabel *m_label = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(h_label), 0, CGRectW(numberView)-CGRectW(h_label)-CGRectW(t_label)-20, 44)];
    //m_label.backgroundColor = __TestOColor;
    m_label.font = MFont(12);
    m_label.textColor = __DefaultColor;
    m_label.textAlignment = NSTextAlignmentCenter;
    m_label.text= _goodsNum;
    [numberView addSubview:m_label];
}

#pragma mark - ===== bottom btn Action =====
- (void)siftRestBtnAction {
    NSLog(@"重置操作");
    
    _chooseArray = [NSMutableArray array];
    NSLog(@"%d",self.chooseArray.count);
    
    
    [middleView removeFromSuperview];
    [bottomView removeFromSuperview];
    [self initSubView:bgView];
    
    
}


- (void)siftSureBtnAction {
    NSLog(@"完成筛选");
    NSLog(@"%d",self.chooseArray.count);
    [self setHidden:YES];
}


- (void)newChooseBtnAciton:(UIButton *)btn {
    
    if (btn.selected == YES) {

        btn.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
        btn.selected = NO;
        
        //移除选择
        NSString *btnTitle = btn.titleLabel.text;
        [self.chooseArray removeObject:btnTitle];
        
        
    }else { //选中情况 按钮框 字体变绿 图片 出现绿色对勾
        if (btn.tag-50 != 0) {//判断 不是第一个按钮时
            b_allBtn.selected = NO;
            b_allBtn.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
            NSString *btnTitle = btn.titleLabel.text;
            [self.chooseArray addObject:btnTitle];
        }else{
            btn.layer.borderColor = __DefaultColor.CGColor;
            
  
        }
        
        btn.selected = YES;
        
    }
    
    
}




//- (void)showSiftView {
//    UIWindow *win = [[UIApplication sharedApplication] keyWindow];
//    UIView *topView = [win.subviews firstObject];
//    self.alpha = 0;
//    [topView addSubview:self];
//    //self.backgroundColor = [UIColor blackColor];
//    [UIView animateWithDuration:0.3 animations:^{
//        self.alpha = 1.0;
//    }];
//}
//
//- (void)siftViewHidden {
//    [UIView animateWithDuration:0.3 animations:^{
//        self.alpha = 0;
//    } completion:^(BOOL finished) {
//        [self removeFromSuperview];
//    }];
//}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
