//
//  ZCMyOrderSuccessPayFooter.h
//  shopSN
//
//  Created by yisu on 16/7/14.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "BaseView.h"

@interface ZCMyOrderSuccessPayFooter : BaseView

/** 交易成功订单 商品数量 */
@property (nonatomic, assign) NSInteger sp_shopCount;


/** 交易成功订单 价格 */
@property (nonatomic, strong) UILabel *sp_settleNowPriceLb;

/** 交易成功订单 数量 */
@property (nonatomic, strong) UILabel *sp_settleGoodsCountLb;


/** 交易成功订单 评价 按钮*/
@property (nonatomic, strong) UIButton *sp_judgeBtn;

/** 交易成功订单 删除订单 按钮*/
@property (nonatomic, strong) UIButton *sp_deleteOrderBtn;

/** 交易成功订单 查看物流 按钮*/
@property (nonatomic, strong) UIButton *sp_logistSituationBtn;



@end
