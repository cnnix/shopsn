//
//  ZListGoodsCell.h
//  shopSN
//
//  Created by yisu on 16/9/3.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 会员商品 页面
 *
 *   商品类 cell
 *
 */
#import "BaseTableViewCell.h"

@interface ZListGoodsCell : BaseTableViewCell

/** cell 图片 */
@property (nonatomic, strong) UIImageView *iconIV;


/** cell 详情 */
@property (nonatomic, strong) UILabel *detailLb;

/** cell 价格 */
@property (nonatomic, strong) UILabel *priceLb;


/** cell 积分 */
@property (nonatomic, strong) UILabel *pointLb;




/** 添加会员商品信息 */
- (void)setGoodsInfo:(ZGoodsModel *)goods;

@end
