//
//  ZCommonGoodsIntroductionCell.m
//  shopSN
//
//  Created by yisu on 16/8/1.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCommonGoodsIntroductionCell.h"

#import "ZCommonGoodsHeader.h"
//#import "ZHeaderScrollViewController.h"
//#import "ZHeadImage.h"

@interface ZCommonGoodsIntroductionCell ()
{
    UIView *bgView;
    NSArray *headerImageArray;//顶部图片数组
    //UIView *headerView;       //顶部视图
    ZCommonGoodsHeader *headerView;
    
}

//@property (nonatomic, strong) ZHeaderScrollViewController *headerScrollViewController;//顶部滚动广告视图




@end

@implementation ZCommonGoodsIntroductionCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        //点击cell不变色
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        
        [self initSubViews];
    }
    return self;
}


- (void)initSubViews {
    //背景
    bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, 530)];
    [self addSubview:bgView];
    //    bgView.backgroundColor = [UIColor orangeColor];
    
    
    //1 图片
//    _goodsIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(bgView), 200)];
//    [bgView addSubview:_goodsIV];
//    _goodsIV.contentMode = UIViewContentModeScaleAspectFit;
//    _goodsIV.image = MImage(@"pic11.png");
//    //self.goodsIV = goodsIV;
    
    
    //1 滚动广告视图
//    headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(bgView),300)];
//    [bgView addSubview:headerView];
//    headerView.backgroundColor = __TestOColor;
    
    headerView = [[ZCommonGoodsHeader alloc] initWithFrame:CGRectMake(0, 10, CGRectW(bgView), 350)];
    [bgView addSubview:headerView];
    
    
    
    
    
    
    
    //4 文字描述
    // 文字测试 妈咪宝贝瞬吸干爽纸尿裤/尿不湿L138片 女宝宝尤妮佳旗下
    _goodsDetailLb = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectYH(headerView)+10, CGRectW(bgView)-20, 48)];
    [bgView addSubview:_goodsDetailLb];
    //_goodsDetailLb.backgroundColor = __TestGColor;
    _goodsDetailLb.numberOfLines = 0;
    _goodsDetailLb.font = MFont(15);
    _goodsDetailLb.textColor = HEXCOLOR(0x333333);
    _goodsDetailLb.text = @"妈咪宝贝瞬吸干爽纸尿裤/尿不湿L138片 女宝宝尤妮佳旗下";
    //self.goodsDetailLb = _goodsDetailLb;
    
    
    
    //5 当前价格
    _goodsNowPriceLb = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectYH(_goodsDetailLb)+10, 75, 20)];
    [bgView addSubview:_goodsNowPriceLb];
    
    //    _goodsNowPriceLb.backgroundColor = __TestGColor;
    _goodsNowPriceLb.font = MFont(11);
    _goodsNowPriceLb.textColor = __MoneyColor;
    //_goodsNowPriceLb.textAlignment = NSTextAlignmentRight;
    _goodsNowPriceLb.text = @"￥189.00";
    //self.goodsNowPriceLb = _goodsNowPriceLb;
    //[self setNowPriceLbtextAttributes:self.goodsNowPriceLb.text];
    
    
    
    //6 促销
//    UIView *promotionView = [[UIView alloc] initWithFrame:CGRectMake(CGRectXW(_goodsNowPriceLb)+5, CGRectYH(_goodsDetailLb)+15, 24, 12)];
//    [bgView addSubview:promotionView];
//    promotionView.backgroundColor = __TestOColor;
//    promotionView.layer.cornerRadius = 2.5f;
//    
//    UILabel *promotionLb = [[UILabel alloc] initWithFrame:CGRectMake(2, 1, 20, 10)];
//    [promotionView addSubview:promotionLb];
//    //    promotionLb.backgroundColor = __TestGColor;
//    //    promotionLb.layer.cornerRadius = 2.0f;
//    //    promotionLb.layer.borderWidth = 1.0;
//    //    promotionLb.layer.borderColor = __TestGColor.CGColor;
//    promotionLb.font = MFont(9);
//    promotionLb.textColor = HEXCOLOR(0xffffff);
//    promotionLb.textAlignment = NSTextAlignmentCenter;
//    promotionLb.text = @"促销";
    
    
    //7 原始价格
    _goodsOriginalPriceLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_goodsNowPriceLb)+5, CGRectYH(_goodsDetailLb)+17, 85, 10)];
    [bgView addSubview:_goodsOriginalPriceLb];
    //    _goodsOriginalPriceLb.backgroundColor = __TestGColor;
    _goodsOriginalPriceLb.font = MFont(10);
    _goodsOriginalPriceLb.textColor = HEXCOLOR(0x999999);
    _goodsOriginalPriceLb.text = @"￥249.00";
    //self.goodsOriginalPriceLb = _goodsOriginalPriceLb;
    //[self setOranginalPriceLbtextAttributes:self.goodsOranginalPriceLb.text];
    
    
    //分享和收藏按钮
    _goodsShareButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectW(bgView)-50-10, CGRectYH(_goodsDetailLb)+10, 50, 25)];
    [bgView addSubview:_goodsShareButton];
    _goodsShareButton.backgroundColor = [UIColor groupTableViewBackgroundColor];
    //[_goodsShareButton setImage:MImage(@"ic_share.png") forState:BtnNormal];
    _goodsShareButton.titleLabel.font = MFont(12);
    _goodsShareButton.layer.borderWidth = 1.0;
    _goodsShareButton.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
    [_goodsShareButton setTitle:@"分享" forState:BtnNormal];
    [_goodsShareButton setTitleColor:HEXCOLOR(0x333333) forState:BtnNormal];
    
    
    
    _goodsCollectButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectW(bgView)-120, CGRectYH(_goodsDetailLb)+10, 50, 25)];
    [bgView addSubview:_goodsCollectButton];
    _goodsCollectButton.backgroundColor = [UIColor groupTableViewBackgroundColor];
    //[_goodsCollectButton setImage:MImage(@"ic_collect.png") forState:BtnNormal];
    _goodsCollectButton.titleLabel.font = MFont(12);
    _goodsCollectButton.layer.borderWidth = 1.0;
    _goodsCollectButton.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
    [_goodsCollectButton setTitle:@"收藏" forState:BtnNormal];
    [_goodsCollectButton setTitleColor:HEXCOLOR(0x333333) forState:BtnNormal];
    
    
    
    
    //8 客服内容
    //_goodsNowPriceLb = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectYH(_goodsDetailLb)+10, 65, 20)];
    UIView *serviceView = [[UIView alloc] initWithFrame:CGRectMake(10, CGRectYH(_goodsNowPriceLb)+5, CGRectW(bgView)-20, 60)];
    [bgView addSubview:serviceView];
    //serviceView.backgroundColor = __TestGColor;
    
    //8.1 pic
    UIImageView *phoneIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 20, 25, 25)];
    [serviceView addSubview:phoneIV];
    //phoneIV.contentMode = UIViewContentModeScaleAspectFit;
    //phoneIV.image = MImage(@"dianhua_m.png");
    phoneIV.image = MImage(@"TEL3.png");
    
    //电话按钮
    UIButton *phoneCallBtn = [[UIButton alloc] initWithFrame:phoneIV.frame];
    [serviceView addSubview:phoneCallBtn];
    [phoneCallBtn addTarget:self action:@selector(phoneCallButtonAction) forControlEvents:BtnTouchUpInside];
    
    
    //8.2 phoneLb
//    UILabel *phoneLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(phoneIV)+10, 10, CGRectW(serviceView)-CGRectW(phoneIV)-20, 20)];
    UILabel *phoneLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(phoneIV)+10, 0, CGRectW(serviceView)-CGRectW(phoneIV)-20, 60)];
    [serviceView addSubview:phoneLb];
    //phoneLb.backgroundColor = __TestOColor;
    phoneLb.font = MFont(15);
    phoneLb.textColor = HEXCOLOR(0x333333);
    phoneLb.numberOfLines = 0;
    //phoneLb.text = @"客服电话：[ 021 - 52218886 ]";
    phoneLb.text = @"[021-52218886] 客服时间：[09:30 - 18:30]";
    
    //8.2 timeLb
//    UILabel *timeLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(phoneIV)+10, CGRectYH(phoneLb)+5, CGRectW(serviceView)-CGRectW(phoneIV)-20, 20)];
//    [serviceView addSubview:timeLb];
//    timeLb.font = MFont(14);
//    timeLb.textColor = HEXCOLOR(0x333333);
//    timeLb.text = @"客服时间：[  09:30 -- 18:30  ]";
    
    
    
    
    //9 底部线条
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectH(bgView)-10, CGRectW(bgView), 10)];
    [bgView addSubview:lineIV];
    lineIV.backgroundColor = HEXCOLOR(0xf0f0f0);
    
}


#pragma mark - 拨打电话
- (void)phoneCallButtonAction {
    NSString *allStr = @"tel:02152218886";
    
    NSLog(@"phoneNum:%@",allStr);
    allStr = [allStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:allStr]];
    
}


#pragma mark - reloadData
- (void)setGoodsIntrContent:(ZGoodsDeal *)deal {
    
    //detail
    self.goodsDetailLb.text = deal.goodsTitle;
    
    //nowPrice
    self.goodsNowPriceLb.text = [NSString stringWithFormat:@"￥%@元",deal.goodsNowPrice];
       
    [self setNowPriceLbtextAttributes:self.goodsNowPriceLb];
    
    //originalPrice
    self.goodsOriginalPriceLb.text = [NSString stringWithFormat:@"￥%@元",deal.goodsOriginalPrice];
    [self setOriginalPriceLbtextAttributes:self.goodsOriginalPriceLb];
    
    
    //imageArray
    //判断
    if ([deal.goodsIntrPicArray isKindOfClass:[NSNull class]]) {
        NSLog(@"没有数据");
        [headerView getDataWithImageArray:@[]];
    }else{
        [headerView getDataWithImageArray:deal.goodsIntrPicArray];
    }

    
    
    
}

#pragma mark - labelText Settting
//设置当前价格label字体变化
- (void)setNowPriceLbtextAttributes:(UILabel *)label {
    NSString *text = label.text;
    NSUInteger length = [text length];
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:text];
    //中间字符大小
    [attri addAttribute:NSFontAttributeName value:MFont(16) range:NSMakeRange(1, length-2)];
    
    [label setAttributedText:attri];
    
}

//设置原始价格label字体变化
- (void)setOriginalPriceLbtextAttributes:(UILabel *)label {
    NSString *text = label.text;
    NSUInteger length = [text length];
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:text];
    //添加删除线
    [attri addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:NSMakeRange(0, length)];
    
    [label setAttributedText:attri];
    
}


//加载广告图片
//- (void)addHeaderImage:(NSArray *)array {
//    NSMutableArray *mutArray = [NSMutableArray array];
//    for (int i=0; i<array.count; i++) {
//        ZHeadImage *headImage = [[ZHeadImage alloc] init];
//        headImage.imageName = array[i];
//        [mutArray addObject:headImage];
//    }
//    headerImageArray = mutArray;
//    
//    
//    //添加滚动视图
//    _headerScrollViewController = [[ZHeaderScrollViewController alloc] init];
//    
//    
//    CGFloat width  = headerView.frame.size.width;
//    CGFloat height = headerView.frame.size.height;//调整滚动视图图片高度
//    
//    _headerScrollViewController.contentSize = CGSizeMake(width, height);
//    _headerScrollViewController.view.frame = CGRectMake(0, 0, width, height);
//    [headerView addSubview:self.headerScrollViewController.view];
//    
//    
//    [self.headerScrollViewController reloadPicData:headerImageArray];
//    
//}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
