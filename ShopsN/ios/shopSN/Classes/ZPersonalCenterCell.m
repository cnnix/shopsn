//
//  ZPersonalCenterCell.m
//  shopSN
//
//  Created by chang on 16/7/6.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZPersonalCenterCell.h"
#import "ZPCMyManagerSubView.h"

@interface ZPersonalCenterCell ()
{
    UIView *_titleView;
}

@property (nonatomic, strong)ZPCMyManagerSubView *managerSubView;

/**管理行 按钮 */
@property (nonatomic, strong) UIButton *managerButton;


@end

@implementation ZPersonalCenterCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.backgroundColor = HEXCOLOR(0xdedede);
        
        [self initSubViews];
    }
    
    return self;
}



- (void)initSubViews {
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, 140)];
    [self addSubview:bgView];
    bgView.backgroundColor = HEXCOLOR(0xffffff);
    
    //标题 个人信息 账号管理 会员资格 订单管理 我的收藏 分享shopSN 我的足迹 我的shopSN
    //图片 gwxx    zhgl    hyzg   ddgl    wdsc   fxzzy     zj      wdzzy
    NSArray *titleLbArray = @[@"个人信息", @"账号管理", @"订单管理", @"我的收藏", @"我的足迹",@"版权申明"];
    NSArray *titleIVArray = @[@"gwxx.png", @"zhgl.png", @"ddgl.png", @"wdsc.png",  @"zj.png",@"zhgl.png"];
    
    for (int i=0; i<titleLbArray.count; i++) {
        
        //分配view
        //两边间距10，中间有两个间距10，剩余空间除3  每行高70 共3行
        _titleView = [[UIView alloc] initWithFrame:CGRectMake((i%3)*(CGRectW(bgView)-40)/3+(i%3)*10+10, (i/3)*70, (CGRectW(bgView)-40)/3, 69)];
        [bgView addSubview:_titleView];
//        _titleView.backgroundColor = __TestGColor;
//        NSLog(@"w:%lf===h:%lf", _titleView.frame.size.width, _titleView.frame.size.height);
        
        //添加子视图
        _managerSubView = [[ZPCMyManagerSubView alloc] initWithFrame:CGRectMake((CGRectW(_titleView)-80)/2, 5, 80, 60)];
        [_titleView addSubview:_managerSubView];
//        _managerSubView.backgroundColor = __TestOColor;
        _managerSubView.titleIV.image = MImage(titleIVArray[i]);
        _managerSubView.titleLb.text = titleLbArray[i];
        
        //添加同尺寸按钮
        _managerButton = [[UIButton alloc] initWithFrame:_managerSubView.frame];
        [_titleView addSubview:_managerButton];
        _managerButton.tag = 100+i;
        [_managerButton addTarget:self action:@selector(managerButtonAction:) forControlEvents:BtnTouchUpInside];
        
    }
    
    //每行的底部线条
    for (int j=0; j<2; j++) {
        UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, (j+1)*69+j*1, __kWidth, 1)];
        [bgView addSubview:lineIV];
        lineIV.backgroundColor = HEXCOLOR(0xdedede);
    }
    
}

#pragma mark- ===== 按钮触发方法 =====
//管理行按钮触发方法
- (void)managerButtonAction:(UIButton *)sender {
    
    [self.delegate didManageButton:sender];
    
    //test
//    switch (sender.tag-100) {
//        case 0:
//        {
//            NSLog(@"进入 个人信息页面");
//        }
//            break;
//            
//        case 7:
//        {
//            NSLog(@"进入 我的shopSN页面");
//        }
//            break;
//            
//        default:
//            break;
//    }
    
    
}




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
