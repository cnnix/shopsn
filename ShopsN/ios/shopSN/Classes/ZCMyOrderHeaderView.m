//
//  ZCMyOrderHeaderView.m
//  shopSN
//
//  Created by yisu on 16/7/14.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCMyOrderHeaderView.h"

@implementation ZCMyOrderHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        
        //1 背景
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [self addSubview:bgView];
//        bgView.backgroundColor = __TestOColor;
        
        //2 上边框 view
        UIView *topView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(bgView), 10)];
        [bgView addSubview:topView];
        topView.backgroundColor = HEXCOLOR(0xf0f0f0);
        
        //3 订单信息
        _orderDateLb = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectYH(topView), CGRectW(bgView)-100-20, 30)];
        [bgView addSubview:_orderDateLb];
//    _orderDateLb.backgroundColor = __TestOColor;
        _orderDateLb.font = MFont(12);
        _orderDateLb.textColor = HEXCOLOR(0x333333);
        _orderDateLb.textAlignment = NSTextAlignmentLeft;
        _orderDateLb.text = @"2016-05-05";
        
        //4 订单类型
        _orderTypeLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(bgView)-10-100, CGRectYH(topView), 100, 30)];
        [bgView addSubview:_orderTypeLb];
        _orderTypeLb.font = MFont(12);
        _orderTypeLb.textColor = HEXCOLOR(0x333333);
        _orderTypeLb.textAlignment = NSTextAlignmentRight;
        _orderTypeLb.text = @"待付款";
        
        //5 边线1
        UIImageView *lineIV1 = [[UIImageView alloc] initWithFrame:CGRectMake(10, CGRectH(bgView)-1, CGRectW(bgView)-10, 1)];
        [bgView addSubview:lineIV1];
        lineIV1.backgroundColor = HEXCOLOR(0xdedede);
        
        
    }
    return self;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
