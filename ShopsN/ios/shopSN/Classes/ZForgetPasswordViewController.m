//
//  ZForgetPasswordViewController.m
//  shopSN
//
//  Created by yisu on 16/6/13.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZForgetPasswordViewController.h"
#import "ZRetrievePasswordViewController.h"

#import "ZYSRegisterViewController.h"

@interface ZForgetPasswordViewController ()<UITextFieldDelegate>
{
    UIView *messageBGView;//提示信息
}
/** 手机号 */
@property (nonatomic, strong) UITextField *phoneTF;

@property (nonatomic) BOOL isSms;

@end

@implementation ZForgetPasswordViewController

#pragma mark - 隐藏标签栏
- (void)viewWillAppear:(BOOL)animated {
    self.tabBarController.tabBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    self.tabBarController.tabBar.hidden = NO;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = HEXCOLOR(0xf0f0f0);
    
    self.title = @"忘记密码";
    
    [self initView];//初始化视图
    
}


#pragma mark - ===== 视图初始化 =====
//初始化视图
- (void)initView {
    
    //mainView
    UIView *mainView = [[UIView alloc] initWithFrame:CGRectMake(0, 64+8, __kWidth, 44)];
    mainView.backgroundColor = HEXCOLOR(0xffffff);
    [self.view addSubview:mainView];
    
    //1 图片
    UIImageView *titleIV = [[UIImageView alloc] initWithFrame:CGRectMake(15, 11, 22, 22)];
    [mainView addSubview:titleIV];
    titleIV.contentMode = UIViewContentModeScaleAspectFit;
    titleIV.image = MImage(@"zc_sj");

    
    
    //2 文本框
    UITextField *phoneTF = [[UITextField alloc] initWithFrame:CGRectMake(CGRectXW(titleIV)+10, 0, CGRectW(mainView)-30-CGRectXW(titleIV)-10, 44)];
    //phoneTF.backgroundColor = __TestOColor;
    phoneTF.font = MFont(14);
    phoneTF.textColor = HEXCOLOR(0x333333);
    phoneTF.placeholder = @"请输入要找回的手机号码";
    [mainView addSubview:phoneTF];
    _phoneTF = phoneTF;
    _phoneTF.delegate = self;
    
    
    
    //按钮
    UIButton *retrieveBtn = [[UIButton alloc] initWithFrame:CGRectMake(15, CGRectYH(mainView)+15, __kWidth-30, 44)];
    retrieveBtn.backgroundColor = __DefaultColor;
    retrieveBtn.titleLabel.font = MFont(16);
    retrieveBtn.layer.cornerRadius = 5;
    [retrieveBtn setTitle:@"找回密码" forState:BtnNormal];
    [retrieveBtn setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
    [retrieveBtn addTarget:self action:@selector(retrieveBtnAction) forControlEvents:BtnTouchUpInside];
    [self.view addSubview:retrieveBtn];
    
}

#pragma mark - ===== 按钮方法 =====
//找回密码
- (void)retrieveBtnAction {
    
    
    
    //判断输入内容是否正确
    [self checkTextFieldContent];
    
    
    [self textFieldResignFirstResponder];
    
    //找回密码请求
    [self retrievePasswordRequest];
    
    
    
}


//判断输入内容是否正确
- (void)checkTextFieldContent {
    
    if (IsNilString(_phoneTF.text)) {
        [SXLoadingView showAlertHUD:@"手机号码不能为空" duration:SXLoadingTime];
        return;
    }
    if (_phoneTF.text.length!=11) {
        [SXLoadingView showAlertHUD:@"手机号码不正确" duration:SXLoadingTime];
        return;
    }
    /**
     * 移动号段正则表达式
     */
    NSString *CM_NUM = @"^((13[4-9])|(147)|(15[0-2,7-9])|(178)|(18[2-4,7-8]))\\d{8}|(1705)\\d{7}$";
    /**
     * 联通号段正则表达式
     */
    NSString *CU_NUM = @"^((13[0-2])|(145)|(15[5-6])|(176)|(18[5,6]))\\d{8}|(1709)\\d{7}$";
    /**
     * 电信号段正则表达式
     */
    NSString *CT_NUM = @"^((133)|(153)|(177)|(18[0,1,9]))\\d{8}$";
    NSPredicate *pred1 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM_NUM];
    BOOL isMatch1 = [pred1 evaluateWithObject:_phoneTF.text];
    NSPredicate *pred2 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU_NUM];
    BOOL isMatch2 = [pred2 evaluateWithObject:_phoneTF.text];
    NSPredicate *pred3 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT_NUM];
    BOOL isMatch3 = [pred3 evaluateWithObject:_phoneTF.text];
    if (!(isMatch1||isMatch2||isMatch3)) {
        [SXLoadingView showAlertHUD:@"手机号码不正确" duration:SXLoadingTime];
        return;
    }
    
    
    
}

//找回密码请求
- (void)retrievePasswordRequest {
    
    // 115.159.146.202/api/api.php?action=check_mobile&mobile=18808051113
    NSDictionary *params =@{@"mobile":_phoneTF.text};
    
    [ZHttpRequestService POST:@"Login/forgetPassword" withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            
//            NSArray *data=jsonDic[@"data"];
//            NSDictionary *dic=data[0];
//            NSString *userid=[Utils getTextStrByText:dic[@"id"]];
            //跳转至修改密码页面
            [self toRetrievePasswordPageWithPhone:self.phoneTF.text];
//            
        }else{
            [self showMessageView];
        }
    } failure:^(NSError *error) {
        [SXLoadingView showAlertHUD:@"找回密码请求失败" duration:SXLoadingTime];
        //NSLog(@"%@",error);
    } animated:NO withView:nil];

    
  
    
    
}

//跳转至修改密码页面
- (void)toRetrievePasswordPageWithPhone:(NSString *)phone {
    
    NSLog(@"跳转至 找回密码页面");
    ZRetrievePasswordViewController *vc = [[ZRetrievePasswordViewController alloc] init];
    vc.phoneStr = phone;
    
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - 非注册用户提示信息
- (void)showMessageView {
    NSLog(@"显示信息提示");
    
    //1 弹出框背景
    messageBGView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, __kHeight)];
    messageBGView.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.5f];
    [self.view addSubview:messageBGView];
    
    
    //2 菜单
    UIView *messageView = [[UIView alloc] initWithFrame:CGRectMake((CGRectW(messageBGView)-260)/2, (CGRectH(messageBGView)-120)/2, 260, 120)];
    [messageBGView addSubview:messageView];
    messageView.backgroundColor = HEXCOLOR(0xffffff);
    //messageView.layer.cornerRadius = 5.0f;
    
    //titleView
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(messageView), 30)];
    [messageView addSubview:titleView];
    titleView.backgroundColor = HEXCOLOR(0xf1f1f1);
    
    //titleLb
    UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 100, 30)];
    [titleView addSubview:titleLb];
    titleLb.font = MFont(12);
    titleLb.textColor = HEXCOLOR(0x333333);
    titleLb.text = @"信息";
    
    //cancelBtn
    UIButton *cancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectW(titleView)-30, 5, 20, 20)];
    [titleView addSubview:cancelBtn];
    [cancelBtn setImage:MImage(@"close") forState:BtnNormal];
    [cancelBtn addTarget:self action:@selector(messageCancelAction) forControlEvents:BtnTouchUpInside];
    
    //descView
    UIView *descView = [[UIView alloc] initWithFrame:CGRectMake((CGRectW(messageView)-220)/2, CGRectYH(titleView)+15, 220, 20)];
    [messageView addSubview:descView];
    //descView.backgroundColor = __TestOColor;
    
    
    //iconIV
    UIImageView *iconIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [descView addSubview:iconIV];
    iconIV.contentMode = UIViewContentModeScaleAspectFit;
    iconIV.image = MImage(@"jz.png");
    
    //descLb
    UILabel *descLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(iconIV)+10, 5, 182, 15)];
    [descView addSubview:descLb];
    //descLb.backgroundColor = __TestGColor;
    descLb.font = MFont(13);
    descLb.textColor = HEXCOLOR(0x333333);
    descLb.textAlignment = NSTextAlignmentCenter;
    descLb.text = @"您的账号还未注册过，请先注册";
    
    //messageBtn color(85 188 225)(0x55bce1)
    UIButton *messageBtn = [[UIButton alloc] initWithFrame:CGRectMake((CGRectW(messageView)-80)/2, CGRectH(messageView)-40, 80, 30)];
    [messageView addSubview:messageBtn];
    messageBtn.backgroundColor = HEXCOLOR(0x55bce1);
    messageBtn.titleLabel.font = MFont(12);
    [messageBtn setTitle:@"确定" forState:BtnNormal];
    [messageBtn setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
    [messageBtn addTarget:self action:@selector(messageSureAction) forControlEvents:BtnTouchUpInside];
    
}

//信息提示框取消按钮触发方法
- (void)messageCancelAction {
    NSLog(@"取消信息提示");
    //清除遮罩视图
    [messageBGView removeFromSuperview];
}

//信息提示框确认按钮触发方法
- (void)messageSureAction {
    NSLog(@"确认信息提示");
    
    //清除遮罩视图
    [messageBGView removeFromSuperview];
    
    //跳转至注册页面
    [self getSMS];
    
}
//短信验证开通
-(void)getSMS{
    [ZHttpRequestService POST:@"Login/isOpenSms" withParameters:nil success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            NSArray *data=jsonDic[@"data"];
            if (data.count) {
                NSDictionary *dic=data[0];
                NSString *str = dic[@"sms_open"];
                if ([str isEqualToString:@"0"]) {
                    _isSms = YES;
                }else{
                    _isSms = NO;
                }
                ZYSRegisterViewController *vc = [[ZYSRegisterViewController alloc]init];
                vc.isSms = _isSms;
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
    } failure:^(NSError *error) {

    } animated:YES withView:nil];
}


#pragma mark - ===== 移除键盘 =====
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (_phoneTF) {
        [self.view endEditing:YES];
    }
}




#pragma mark -UITextFiledDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


-(void)textFieldResignFirstResponder{
    [self.view endEditing:YES];
}

#pragma mark - others
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
