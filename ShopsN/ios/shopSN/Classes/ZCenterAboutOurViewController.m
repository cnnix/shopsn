//
//  ZCenterAboutOurViewController.m
//  shopSN
//
//  Created by yisu on 16/10/8.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCenterAboutOurViewController.h"

@interface ZCenterAboutOurViewController ()

@end

@implementation ZCenterAboutOurViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLb.text = @"关于我们";
    
    [self initSubViews:self.mainMiddleView];//初始化中间视图
    
}


#pragma mark - 中间视图部分 相关子视图
//初始化相关子视图
- (void)initSubViews:(UIView *)view {
    
    
    //图片
    UIImageView *iconIV = [[UIImageView alloc] initWithFrame:CGRectMake((CGRectW(view)-70)/2, CGRectH(view)/4, 70, 70)];
    [view addSubview:iconIV];
    iconIV.image = MImage(@"80.png");
    
    //版本信息
    UILabel *verLb = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectYH(iconIV)+10, CGRectW(view)-20, 30)];
    [view addSubview:verLb];
    verLb.textColor = HEXCOLOR(0x666666);
    verLb.textAlignment = NSTextAlignmentCenter;

    verLb.text = [NSString stringWithFormat:@"%@ %@ 1.0",simpleTitle,maintitlepy];
    
    //版权信息
#warning 版权信息需要手动更改
    UILabel *copyright_ELb = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectH(view)-90, CGRectW(view)-20, 30)];
    [view addSubview:copyright_ELb];
    copyright_ELb.font = MFont(14);
    copyright_ELb.textAlignment = NSTextAlignmentCenter;
    copyright_ELb.textColor = HEXCOLOR(0x666666);
    copyright_ELb.text = CopyRight;
    
    UILabel *copyright_CLb = [[UILabel alloc] initWithFrame:CGRectMake(5, CGRectH(view)-60, CGRectW(view)-10, 30)];
    [view addSubview:copyright_CLb];
    copyright_CLb.font = MFont(14);
    copyright_CLb.textAlignment = NSTextAlignmentCenter;
    copyright_CLb.textColor = HEXCOLOR(0x666666);
    copyright_CLb.text = [NSString stringWithFormat:@"%@ 版权所有",MainTitle];
    
}




#pragma mark - others
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
