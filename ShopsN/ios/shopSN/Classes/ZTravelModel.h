//
//  ZTravelModel.h
//  shopSN
//
//  Created by yisu on 16/9/12.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 会员商品 相关页面
 *
 *  旅游模型类
 *
 */


#import <Foundation/Foundation.h>

@interface ZTravelModel : NSObject

/** 旅游模型类 旅游商品id */
@property (copy, nonatomic) NSString *tourismID;

/** 旅游模型类 旅游商品标题 */
@property (copy, nonatomic) NSString *tourismTitle;

/** 旅游模型类 旅游商品图片地址 */
@property (copy, nonatomic) NSString *tourismPicUrl;

/** 旅游模型类 旅游商品原价 */
@property (copy, nonatomic) NSString *tourismOriginalPrice;

/** 旅游模型类 旅游商品现价 */
@property (copy, nonatomic) NSString *tourismNowPrice;

/** 旅游模型类 旅游商品积分 */
@property (copy, nonatomic) NSString *tourismPoints;



@end
