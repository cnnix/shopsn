//
//  ZAccountManagerModel.m
//  shopSN
//
//  Created by 王子豪 on 16/9/12.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZAccountManagerModel.h"
ZAccountManagerModel *acountModel = nil;
@implementation ZAccountManagerModel

+(instancetype)shareAccountManagerData{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        acountModel = [[ZAccountManagerModel alloc] init];
    });
    return acountModel;
}
@end
