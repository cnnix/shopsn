//
//  ZHomeGirdGoodsFooter2.h
//  shopSN
//
//  Created by yisu on 16/10/14.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 首页页面 collectionView
 *
 *  会员商品展示 footer
 *
 */

#import <UIKit/UIKit.h>
#import "YFRollingLabel.h"
@interface ZHomeGirdGoodsFooter2 : UICollectionReusableView

/** 商品展示 footer 视图 */
@property (nonatomic, strong) UIView *commonGoodsFooterView;


@property (nonatomic, strong) UIScrollView *scrollView;

/** 普通商品展示 footer center图片 */
@property (nonatomic, strong) UIImageView *commonGoodsFooterCenterIV;


@end
