//
//  ZPCMyShareSubView.m
//  shopSN
//
//  Created by chang on 16/7/7.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZPCMyShareSubView.h"

@implementation ZPCMyShareSubView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        //1 标题图片
        _descLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, CGRectW(self), 20)];
        [self addSubview:_descLb];
//        _descLb.backgroundColor = __TestOColor;
        _descLb.font = MFont(13);
        _descLb.textAlignment = NSTextAlignmentCenter;
        
        //2 标题文本
        _titleLb = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectYH(_descLb), CGRectW(self), 20)];
        [self addSubview:_titleLb];
//        _titleLb.backgroundColor = __TestOColor;
        _titleLb.font = MFont(11);
        _titleLb.textAlignment = NSTextAlignmentCenter;
        
    }
    return self;
}




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
