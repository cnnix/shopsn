//
//  ZIncreaseAddressViewController.m
//  shopSN
//
//  Created by imac on 16/7/8.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZIncreaseAddressViewController.h"
#import "ReceiveAddressModel.h"
#import "ZChangeAddressTableViewCell.h"
#import "ZChangeAddressTableViewOtherCell.h"
#import "ZChooseAreaView.h"


@interface ZIncreaseAddressViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UITextViewDelegate>

@property (strong,nonatomic) UITableView *tableView;

@property (strong,nonatomic) ReceiveAddressModel*addressModel;
@property (strong,nonatomic) UILabel *placeholderLb;
/**
 *  地区选择
 */
@property (strong,nonatomic) ZChooseAreaView *pickerView;
/**
 *  地区信息
 */
@property (strong,nonatomic) NSString *addStr;
@property (nonatomic) BOOL isDefault;


@end

@implementation ZIncreaseAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _addressModel = [[ReceiveAddressModel alloc]init];
    self.titleLb.text= @"新建收货地址";
    _isDefault=NO;
    
    [self initView];
}
/**
 *  选择视图
 */
- (void)showPick{
    WK(weakSelf)
    _pickerView = [[ZChooseAreaView alloc]initWithAreaFrame:CGRectMake(0, __kHeight-200, __kWidth, 200)];
    [self.view addSubview:_pickerView];
    _pickerView.backgroundColor = LH_RGBCOLOR(200, 200, 210);
    _pickerView.returntextfileBlock=^(NSString *data){
        _addStr =data;
        NSArray *arr =[data componentsSeparatedByString:@" "];
        weakSelf.addressModel.Province=arr[0];
        weakSelf.addressModel.city = arr[1];
        weakSelf.addressModel.area = arr[2];
        
        NSMutableArray *paArr = [@[] mutableCopy];
        
        for (int idx = 0; idx<6; idx++) {
            if (idx!=3) {
                ZChangeAddressTableViewCell *cell = [weakSelf.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]];
                [paArr addObject:cell.detailTV.text];
                
            }
            if (idx==3) {
                ZChangeAddressTableViewOtherCell *cell = [weakSelf.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]];
                [paArr addObject:cell.addressTF.text];
            }
        }
        NSLog(@"112--%@", paArr);
        weakSelf.addressModel.realname = paArr[0];
        weakSelf.addressModel.mobile = paArr[1];
        weakSelf.addressModel.address = paArr[3];
        weakSelf.addressModel.zipcode = paArr[5];
        
        [ weakSelf.tableView reloadData];

        
    };
    
}
-(void)initView{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, __kWidth, __kHeight-108)];
    [self.view addSubview:_tableView];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = LH_RGBCOLOR(230, 230, 230);
    
    UIButton *saveBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, CGRectYH(_tableView)+1, __kWidth, 43)];
    [self.view addSubview:saveBtn];
    [saveBtn setTitle:@"保存地址" forState:BtnNormal];
    [saveBtn setTitleColor:[UIColor whiteColor] forState:BtnNormal];
    saveBtn.backgroundColor = __DefaultColor;
    [saveBtn addTarget:self action:@selector(saveAdd) forControlEvents:BtnTouchUpInside];
    
}



#pragma mark==保存地址==
-(void)saveAdd{
    NSMutableArray *paArr = [@[] mutableCopy];
    
    for (int idx = 0; idx<6; idx++) {
        if (idx!=3) {
            ZChangeAddressTableViewCell *cell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]];
            [paArr addObject:cell.detailTV.text];
        }
        if (idx==3) {
            ZChangeAddressTableViewOtherCell *cell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]];
            [paArr addObject:cell.addressTF.text];
        }
    }
    
    //    NSLog(@"%@", paArr);
    
    NSDictionary *params =@{@"token":[UdStorage getObjectforKey:Userid],
                            @"realname":paArr[0],
                            @"prov":self.addressModel.Province,
                            @"city":self.addressModel.city,
                            @"dist":self.addressModel.area,
                            @"address":paArr[3],
                            @"status":_isDefault?@1:@0,
                            @"mobile":paArr[1],
                            @"zipcode":paArr[5]};
    NSLog(@"%@", params);
    WK(weakSelf)
    [ZHttpRequestService POST:@"Address/addAddressByUser" withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }else{
            NSString *message = jsonDic[@"message"];
            
            [SXLoadingView showAlertHUD:message duration:2];
        }
    } failure:^(NSError *error) {
        
        [SXLoadingView showAlertHUD:@"失败" duration:SXLoadingTime];
        
    } animated:true withView:nil];
}

#pragma mark -UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 6;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 3) {
        ZChangeAddressTableViewOtherCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TableOhterCell"];
        if (!cell) {
            cell = [[ZChangeAddressTableViewOtherCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TableOhterCell"];
        }
            cell.titleLb.text = @"详细地址";
            cell.addressTF.hidden = NO;
            cell.addressTF.tag = 33;
            _placeholderLb = [[UILabel alloc]initWithFrame:CGRectMake(98, 12, __kWidth-133, 30)];
            [cell addSubview:_placeholderLb];
             cell.addressTF.delegate =self;
            _placeholderLb.backgroundColor = [UIColor clearColor];
            _placeholderLb.textColor =LH_RGBCOLOR(200, 200, 200);
            _placeholderLb.textAlignment = NSTextAlignmentLeft;
            _placeholderLb.font = MFont(13);
            _placeholderLb.text = @"请输入详细地址(5~120个字)";
             if (!IsNilString(cell.addressTF.text)) {
                _placeholderLb.hidden = YES;
            }
        
        return cell;
        
    }
    
    ZChangeAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZChangeAddressTableViewCell"];
    if (!cell) {
        cell = [[ZChangeAddressTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ZChangeAddressTableViewCell"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.detailTV.tag = indexPath.row+11;
    cell.detailTV.delegate = self;
    switch (indexPath.row) {
        case 0:
        {
            cell.titleLb.text = @"收货人";
            cell.detailTV.placeholder = @"请输入收货人";
        }
            break;
        case 1:
        {
            cell.titleLb.text = @"手机号码";
            cell.detailTV.placeholder = @"请输入手机号码";
        }
            break;
        case 2:
        {
            cell.titleLb.text = @"省市地区";
            cell.detailTV.placeholder = @"请输入省市地区";
            if (![_addStr isEqualToString:@""]) {
                cell.detailTV.text = _addStr;
            }
        }
            break;
//        case 3:
//        {
//            cell.titleLb.text = @"详细地址";
//            cell.detailTV.hidden = YES;
//            cell.addressTF.hidden = NO;
//            _placeholderLb = [[UILabel alloc]initWithFrame:CGRectMake(3, 0, __kWidth-133, 30)];
//            [cell.addressTF addSubview:_placeholderLb];
//            cell.addressTF.delegate =self;
//            _placeholderLb.backgroundColor = [UIColor clearColor];
//            _placeholderLb.textColor =LH_RGBCOLOR(200, 200, 200);
//            _placeholderLb.textAlignment = NSTextAlignmentLeft;
//            _placeholderLb.font = MFont(13);
//            _placeholderLb.text = @"请输入详细地址(5~120个字)";
//            if (!IsNilString(cell.addressTF.text)) {
//                _placeholderLb.hidden = YES;
//            }
//        }
//            break;
        case 4:
        {
            cell.titleLb.text = @"设为默认地址";
            cell.detailTV.hidden = YES;
            cell.chooseIV.hidden = NO;
            if (_isDefault) {
                cell.chooseIV.image = [UIImage imageNamed:@"yuang02"];
            }else{
                 cell.chooseIV.image = nil;
            }
        }
            break;
        case 5:
        {
            cell.titleLb.text = @"邮编";
            cell.detailTV.placeholder = @"请输入邮政编码";
        }
            break;
        default:
            break;
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==3) {
        return 80;
    }
    return 50;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
      [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 4) {
        _isDefault=!_isDefault;
        NSMutableArray *paArr = [@[] mutableCopy];
        
        for (int idx = 0; idx<6; idx++) {
            if (idx!=3) {
                ZChangeAddressTableViewCell *cell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]];
                [paArr addObject:cell.detailTV.text];
                
            }
            if (idx==3) {
                ZChangeAddressTableViewOtherCell *cell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]];
                [paArr addObject:cell.addressTF.text];
            }
        }
        self.addressModel.realname = paArr[0];
        self.addressModel.mobile = paArr[1];
        self.addressModel.address = paArr[3];
        self.addressModel.zipcode = paArr[5];
        [self.tableView reloadData];
    }
}
#pragma mark -UITextViewDelegate
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if (__kHeight<__k5Height) {
        [UIView animateWithDuration:0.4 animations:^{
            _tableView.frame=CGRectMake(0, 0, __kWidth, __kHeight-130);
        }];
    }
    return YES;
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    if (__kHeight<__k5Height) {
        [UIView animateWithDuration:0.4 animations:^{
            _tableView.frame=CGRectMake(0, 64, __kWidth, __kHeight-130);
        }];
    }
    return YES;
}
-(void)textViewDidChange:(UITextView *)textView{
    if (textView.tag == 33) {
        if (IsNilString(textView.text)) {
            _placeholderLb.hidden = NO;
        }else{
            _placeholderLb.hidden = YES;
        }
    }
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

#pragma mark -UITextFiled Delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField.tag ==13) {
        [self showPick];
        [self.view endEditing:YES];
        return NO;
    }
    if (textField.tag==16) {
        if (__kHeight<__k5Height) {
            [UIView animateWithDuration:0.4 animations:^{
                _tableView.frame = CGRectMake(0, -110, __kWidth, __kHeight-108);
            }];
        }
        if (__kHeight==__k5Height) {
            [UIView animateWithDuration:0.4 animations:^{
                _tableView.frame = CGRectMake(0, 0, __kWidth, __kHeight-108);
            }];
        }
    }
     return YES;
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    if (textField.tag == 16) {
        if (__kHeight<=__k5Height) {
            [UIView animateWithDuration:0.4 animations:^{
                _tableView.frame = CGRectMake(0, 64, __kWidth, __kHeight-108);
            }];
        }
    }
    return YES;
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
