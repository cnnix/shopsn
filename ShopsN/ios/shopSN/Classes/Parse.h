//
//  Parse.h
//  shopSN
//
//  Created by yisu on 16/9/6.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Parse : NSObject



/** 会员商品 一级分类解析 */
+ (NSMutableArray *)parseCategory:(NSArray *)array;

/** 会员商品 二级分类解析 */
+ (NSMutableArray *)parseSubCategory:(NSArray *)array;


/** 会员商品 二级分类解析 */
+ (NSMutableArray *)parseTravelSubCategory:(NSArray *)array;

/** 会员商品 商品列表解析 */
+ (NSMutableArray *)parseGoodsList:(NSArray *)array;



/** 会员商品 详情内容解析 */
+ (NSMutableArray *)parseGoodsDetail:(NSArray *)array;


// 首页数据请求
/** 首页会员商城 banner数据解析 */
+ (NSMutableArray *)parseHomeGoodsBannerData:(NSArray *)array;

/** 首页会员商城 center数据解析 */
+ (NSMutableArray *)parseHomeGoodsCenterData:(NSArray *)array;

/** 首页会员商城 hot数据解析 */
+ (NSMutableArray *)parseHomeGoodsHotData:(NSArray *)array;

/** 首页会员商城 class数据解析 */
+ (NSMutableArray *)parseHomeGoodsClassData:(NSArray *)array;

//商品搜索
/** 首页会员商城 商品搜索解析 */
+ (NSMutableArray *)parseGoodsSearchData:(NSArray *)array;
/** 首页会员商城 商品搜索解析 */
+ (NSMutableArray *)parseTourismSearchData:(NSArray *)array;


/** 商品 套餐内容解析 */
+ (NSMutableArray *)parseDealPakege:(NSArray *)array;


/** 商品 订单内容解析 */
+ (NSMutableArray *)parseGoodsOrders:(NSArray *)array;



@end
