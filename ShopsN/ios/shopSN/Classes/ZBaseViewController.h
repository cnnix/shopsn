//
//  ZBaseViewController.h
//  shopSN
//
//  Created by chang on 16/7/2.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZBaseViewController : UIViewController

/** 导航栏 标题 */
@property (nonatomic, strong) UILabel *titleLb;


/** 导航栏 中间视图 */
@property (nonatomic, strong) UIView *naviMiddleView;

/** 主页面交互菜单 按钮视图 */
@property (nonatomic ,strong) UIView *menuBtnView;


/** 中间 主要内容视图 */
@property (nonatomic, strong) UIView *mainMiddleView;

/** 导航栏 中间视图添加子控件 */
- (void)addNaviSubControllers:(UIView *)middleView;

- (void)backAciton;

@end
