/**
 * 订单js
 */
(function() {
	function Order() {
		this.page = 0;
		/**
		 * ajax 获取订单数据 
		 * @param  string id  form id
		 * @param  int page   页数
		 */
		this.ajaxForMyOrder = function(id, page) {
			this.page = page;
			if (!$('#' + id).length || !this.isNumer(page)) {
				layer.msg('参数错误');
				return false;
			}
			var data = $('#' + id).serialize();
			var url = document.getElementById(id).getAttribute('url');
			this.dataType = '';
			return this.post(url, data, function(res) {
				$("#ajaxGetReturn").html('');
				$("#ajaxGetReturn").append(res);
			})

		}
		/**
		 * 排序 
		 */
		this.sort = function(id, field) {
			$("input[name='order_by']").val(field);
			var v = $("input[name='sort']").val() == 'desc' ? 'asc' : 'desc';
			$("input[name='sort']").val(v);
			this.ajaxForMyOrder(id, this.page);
		}
		
		
		
	}
	Order.prototype = Tool;
	window.Order = new Order();
})(window);

/**
 * 页面加载完成时【运行的方法】 
 */
window.onload = function() {
	Order.ajaxForMyOrder('conditionForm', 1);
}