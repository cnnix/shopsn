<?php
namespace Mobile\Controller;
use Think\Controller;
use Think\Model;

//我的亿速网络
class MyzzyController extends Controller{
	public function _initialize()
	{
		$this->usermodel=M('user');
		$this->membermodel = D('Myzzy');
		$this->huifeimodel = D('Huifei');
		$this->profitmodel=D('Profit');
		$this->proportionmodel=D('Bfb');
		$this->adminmodel = D('Admin');
		//dump(session());exit;
		
		
	
	}
	public function check(){
		$grade_name=M('member','vip_')->where(array('id'=>session('id')))->getField('grade_name');
		//echo $grade_name!="会员";exit;
		if($grade_name=='合伙人'){
			//$this->error('系统维护中。。。。');
		}
		if($grade_name!='会员'){
			//$this->error('对不起，您无权进入此页面或者登陆失效，请重新登陆。');
		}
	}
	public function index(){
		$this->check();
		$model=D('Myzzy');
		$id=session('id');
		$mobile=$model->where(array('id'=>$id))->getField('mobile');
		$pid=$model->where(array('id'=>$id))->getField('pid');
		$myPro=$model->where(array('vip_member.id'=>$id))->join('left join vip_member_profit on vip_member_profit.member_id=vip_member.id')->field('vip_member.id,true_name,mobile,grade_name,surplus_money,add_jf_currency,add_jf_limit')->find();
		
		$myPro['jifen']=$myPro['add_jf_currency']+$myPro['add_jf_limit'];
		$myParent=$model->where(array('id'=>$pid))->data('id,true_name,mobile,grade_name')->find();
		
		$num=$this->td_num($id);
		$this->assign('num',$num);
		$this->assign('id',$id);
		$this->assign('myParent',$myParent);
		$this->assign('myPro',$myPro);
		$this->display();
	}
	public function td_num($id){
		if($id){
			//查询下级所有会员
			$vip=array();
			$my=$this->membermodel->where(array('id'=>$id))->find();
			$huiyuan=$this->membermodel->where(array('id'=>array('neq',$my['id']),'path'=>array('like','%'.$my['id'].'%'),'grade_name'=>'会员'))->select();
			foreach($huiyuan as $v=>$k){
				$arr=explode("-",$k['path']);
				$start=array_search($my['id'],$arr);
				$end=array_search($k['id'],$arr);
				if($end-$start==1){
					$vip[1][]=$k['id'];
				}elseif($end-$start==2){
					$vip[2][]=$k['id'];
				}elseif($end-$start==3){
					$vip[3][]=$k['id'];
				}elseif($end-$start>=4){
					$vip[4][]=$k['id'];
				}
			}
			if(count($vip[1])){
				$vip1=$this->membermodel->where(array('id'=>array('in',$vip[1])))->select();
			}else{
				$vip1=array();
			}
			//dump($vip1);
			//exit;
			if(count($vip[2])){
				$vip2=$this->membermodel->where(array('id'=>array('in',$vip[2])))->select();
			}else{
				$vip2=array();
			}
			if(count($vip[3])){
				$vip3=$this->membermodel->where(array('id'=>array('in',$vip[3])))->select();
			}else{
				$vip3=array();
			}
			//事业合伙人总人数
			$data=$this->membermodel->where(array('id'=>$id))->find();//根据ID查询出用户的所有信息
			$wheres['path']=array('like',$data['path'].'%');//得到用户的路径
			$rows=$this->membermodel->where($wheres)->select();//查询出所有下家
			$myname=$this->membermodel->where(array('id'=>$id))->getField('true_name');
			$zonghehuoren=0;      //下级合伙人总人数
			foreach($rows as $row){
				if($row['path']!=$data['path'] && $row['grade_name']=='合伙人'){
					++$zonghehuoren;
				}
			}
			foreach($rows as &$result){
				$result['path']=str_ireplace($data['path'],'',$result['path']);     //将下家所有的path字段中本身的path进行替换
				if(substr_count( $result['path'],'-')!=0 && $result['grade_name']=='合伙人'){
					if(substr_count( $result['path'],'-')==1){
						$firsts[]=$result;       //下一级所有合伙人
					}elseif(substr_count( $result['path'],'-')==2){
						$seconds[]=$result;      //下2级所有合伙人
					}elseif(substr_count( $result['path'],'-')==3){
						$thirds[]=$result;        //下三级所有合伙人
					}else{
						$others[]=$result;
					}
				}
			}
			foreach($firsts as &$first){
				$first['hehuoren']=$this->jisuan($first['id']);
			}
			foreach($seconds as &$first){
				$second['hehuoren']=$this->jisuan($first['id']);
			}
			foreach($thirds as &$first){
				$third['hehuoren']=$this->jisuan($first['id']);
			}
			
				return array(0=>count($firsts)+count($vip1),1=>count($seconds)+count($vip2),2=>count($thirds)+count($vip3));
		
		}
	}
	private function getIndexData($id){
		$m=D('Myzzy');
		$rst=$m->member_manage($id);
		return $rst;
	}
	public function geren(){
		$this->check();
		$member=D('Myzzy');
		if(IS_AJAX){

			$data['true_name']=str_replace(' ','',I('post.true_name'));
			$data['zip_code']=str_replace(' ','',I('post.zip_code'));
			$data['sex']=I('post.sex');
			$data['email']=str_replace(' ','',I('post.email'));
			$data['kaihuhang']=str_replace(' ','',I('post.kaihuhang'));
			//$data['mobile']=str_replace(' ','',I('post.mobile'));
			$data['card_id']=str_replace(' ','',I('post.card_id'));
			$data['bankid']=str_replace(' ','',I('post.bankid'));
			$data['addr']=I('post.addr');
			$member->startTrans();
			//不予许手机号为空
			//if($data['mobile']==''){
			//	$member->rollback();
			//	$this->ajaxReturn(2);
			//}
			//更新用户登录账户
			$admin_id=$member->where(array('id'=>session('id')))->getField('admin_id');
			$mobile=M('user')->where(array('id'=>session('myUid')))->getField('mobile');
			//if(M('user')->where(array('id'=>array('neq',session('myUid')),'mobile'=>$data['mobile']))->count()){
			//	$rst4=1;
			//}else{
			//	$rst4=0;
			//}
			//if($rst4==1){
			//	$member->rollback();
			//	$this->ajaxReturn(2);
			//}
			//if($mobile!=$data['mobile']){
			//	$rst1=M('user')->where(array('admin_id'=>$admin_id))->save(array('mobile'=>I('post.mobile')));
			//	$rst2=$this->adminmodel->where(array('id'=>$admin_id))->save(array('account'=>I('post.mobile')));
			//}else{
				$rst1=$rst2=true;
			//}
			$rst3=$member->where(array('id'=>session('id')))->save($data);
			if($rst1 && $rst2 && $rst3){
				$member->commit();
				$this->ajaxReturn(1);
			}else{
				$member->rollback();
				$this->ajaxReturn(0);
			}
		}else{
			$row=$member->where(array('id'=>session('id')))->find();//用户自己的相关信息
			$this->assign('my',$row);
			$this->display();
		}

	}
	public function shouyi(){
		$this->check();
		$id=session('id');
		$grade=M('member','vip_')->where(array('id'=>$id))->getField('grade_name');
		$model=D('person');
		$data=$model->where(array('member_id'=>$id))->select();
		//dump($model->getLastSql());
		//$data=$model->query("desc vip_person_month");
		$add_hhr1_num=$add_hhr2_num=$add_hhr3_num=$add_hhr1_money=$add_hhr2_money=$add_hhr3_money=0;
		$hhr1_jmf=$hhr2_jmf=$hhr3_jmf=$jmf_jl_hhr1=$jmf_jl_hhr2=$jmf_jl_hhr3=$add_yk_num=0;
		$add_jf_currency=$add_jf_limit=$add_hy1_num=$add_hy2_num=$add_hy3_num=$add_hy1_money=0;
		$add_hy2_money=$add_hy3_money=$hy1_jmf=$hy2_jmf=$hy3_jmf=$hy1_jf=$hy2_jf=$hy3_jf=0;
		$sxhhr1_hy_num=$sxhhr2_hy_num=$sxhhr3_hy_num=$sxhhr1_hy_jl=$sxhhr2_hy_jl=$sxhhr3_hy_jl=0;
		$sxhhr1_hy_money=$sxhhr2_hy_money=$sxhhr3_hy_money=$sxhhr1_buy_money=$sxhhr1_buy_pay=0;
		$sxhhr2_buy_money=$sxhhr2_buy_pay=$sxhhr3_buy_money=$sxhhr3_buy_pay=$hy1_buy_money=0;
		$hy2_buy_money=$hy3_buy_money=$hy1_buy_pay=$hy2_buy_pay=$hy3_buy_pay=$hy1_tra_money=0;
		$hy2_tra_money=$hy3_tra_money=$hy1_tra_pay=$hy2_tra_pay=$hy3_tra_pay=$my_buy_jf=0;
		$hhr_hytd_buy=$hhr_hytd_tra=$hhr_hytd_zhf=$my_ly_jf=$my_buy_jf=0;
		/* foreach($data as $k=>$v){
			echo '$'.$v["field"].'+=$v["'.$v["field"].'"]<br/>';
		} */
		/* foreach($data as $k=>$v){
			echo '$rows["'.$v["field"].'"]=$'.$v["field"].';<br/>';
		} */
		foreach($data as $k=>$v){
			$add_hhr1_num+=$v["add_hhr1_num"];
			$add_hhr2_num+=$v["add_hhr2_num"];
			$add_hhr3_num+=$v["add_hhr3_num"];
			$add_hhr1_money+=$v["add_hhr1_money"];
			$add_hhr2_money+=$v["add_hhr2_money"];
			$add_hhr3_money+=$v["add_hhr3_money"];
			$hhr1_jmf+=$v["hhr1_jmf"];
			$hhr2_jmf+=$v["hhr2_jmf"];
			$hhr3_jmf+=$v["hhr3_jmf"];
			$jmf_jl_hhr1+=$v["jmf_jl_hhr1"];
			$jmf_jl_hhr2+=$v["jmf_jl_hhr2"];
			$jmf_jl_hhr3+=$v["jmf_jl_hhr3"];
			$add_yk_num+=$v["add_yk_num"];
			$add_jf_currency+=$v["add_jf_currency"];
			$add_jf_limit+=$v["add_jf_limit"];
			$add_hy1_num+=$v["add_hy1_num"];
			$add_hy2_num+=$v["add_hy2_num"];
			$add_hy3_num+=$v["add_hy3_num"];
			$add_hy1_money+=$v["add_hy1_money"];
			$add_hy2_money+=$v["add_hy2_money"];
			$add_hy3_money+=$v["add_hy3_money"];
			$hy1_jmf+=$v["hy1_jmf"];
			$hy2_jmf+=$v["hy2_jmf"];
			$hy3_jmf+=$v["hy3_jmf"];
			$hy1_jf+=$v["hy1_jf"];
			$hy2_jf+=$v["hy2_jf"];
			$hy3_jf+=$v["hy3_jf"];
			$sxhhr1_hy_num+=$v["sxhhr1_hy_num"];
			$sxhhr2_hy_num+=$v["sxhhr2_hy_num"];
			$sxhhr3_hy_num+=$v["sxhhr3_hy_num"];
			$sxhhr1_hy_jl+=$v["sxhhr1_hy_jl"];
			$sxhhr2_hy_jl+=$v["sxhhr2_hy_jl"];
			$sxhhr3_hy_jl+=$v["sxhhr3_hy_jl"];
			$sxhhr1_hy_money+=$v["sxhhr1_hy_money"];
			$sxhhr2_hy_money+=$v["sxhhr2_hy_money"];
			$sxhhr3_hy_money+=$v["sxhhr3_hy_money"];
			$sxhhr1_buy_money+=$v["sxhhr1_buy_money"];
			$sxhhr1_buy_pay+=$v["sxhhr1_buy_pay"];
			$sxhhr2_buy_money+=$v["sxhhr2_buy_money"];
			$sxhhr2_buy_pay+=$v["sxhhr2_buy_pay"];
			$sxhhr3_buy_money+=$v["sxhhr3_buy_money"];
			$sxhhr3_buy_pay+=$v["sxhhr3_buy_pay"];
			$hy1_buy_money+=$v["hy1_buy_money"];
			$hy2_buy_money+=$v["hy2_buy_money"];
			$hy3_buy_money+=$v["hy3_buy_money"];
			$hy1_buy_pay+=$v["hy1_buy_pay"];
			$hy2_buy_pay+=$v["hy2_buy_pay"];
			$hy3_buy_pay+=$v["hy3_buy_pay"];
			$hy1_tra_money+=$v["hy1_tra_money"];
			$hy2_tra_money+=$v["hy2_tra_money"];
			$hy3_tra_money+=$v["hy3_tra_money"];
			$hy1_tra_pay+=$v["hy1_tra_pay"];
			$hy2_tra_pay+=$v["hy2_tra_pay"];
			$hy3_tra_pay+=$v["hy3_tra_pay"];
			$hhr_hytd_buy+=$v["hhr_hytd_buy"];
			$hhr_hytd_tra+=$v["hhr_hytd_tra"];
			$hhr_hytd_zhf+=$v["hhr_hytd_zhf"];
			$my_ly_jf+=$v["my_ly_jf"];
			$my_buy_jf+=$v["my_buy_jf"];
		}
			$rows["add_hhr1_num"]=$add_hhr1_num;
			$rows["add_hhr2_num"]=$add_hhr2_num;
			$rows["add_hhr3_num"]=$add_hhr3_num;
			$rows["add_hhr1_money"]=$add_hhr1_money;
			$rows["add_hhr2_money"]=$add_hhr2_money;
			$rows["add_hhr3_money"]=$add_hhr3_money;
			$rows["hhr1_jmf"]=$hhr1_jmf;
			$rows["hhr2_jmf"]=$hhr2_jmf;
			$rows["hhr3_jmf"]=$hhr3_jmf;
			$rows["jmf_jl_hhr1"]=$jmf_jl_hhr1;
			$rows["jmf_jl_hhr2"]=$jmf_jl_hhr2;
			$rows["jmf_jl_hhr3"]=$jmf_jl_hhr3;
			$rows["add_yk_num"]=$add_yk_num;
			$rows["add_jf_currency"]=$add_jf_currency;
			$rows["add_jf_limit"]=$add_jf_limit;
			$rows["add_hy1_num"]=$add_hy1_num;
			$rows["add_hy2_num"]=$add_hy2_num;
			$rows["add_hy3_num"]=$add_hy3_num;
			$rows["add_hy1_money"]=$add_hy1_money;
			$rows["add_hy2_money"]=$add_hy2_money;
			$rows["add_hy3_money"]=$add_hy3_money;
			$rows["hy1_jmf"]=$hy1_jmf;
			$rows["hy2_jmf"]=$hy2_jmf;
			$rows["hy3_jmf"]=$hy3_jmf;
			$rows["hy1_jf"]=$hy1_jf;
			$rows["hy2_jf"]=$hy2_jf;
			$rows["hy3_jf"]=$hy3_jf;
			$rows["sxhhr1_hy_num"]=$sxhhr1_hy_num;
			$rows["sxhhr2_hy_num"]=$sxhhr2_hy_num;
			$rows["sxhhr3_hy_num"]=$sxhhr3_hy_num;
			$rows["sxhhr1_hy_jl"]=$sxhhr1_hy_jl;
			$rows["sxhhr2_hy_jl"]=$sxhhr2_hy_jl;
			$rows["sxhhr3_hy_jl"]=$sxhhr3_hy_jl;
			$rows["sxhhr1_hy_money"]=$sxhhr1_hy_money;
			$rows["sxhhr2_hy_money"]=$sxhhr2_hy_money;
			$rows["sxhhr3_hy_money"]=$sxhhr3_hy_money;
			$rows["sxhhr1_buy_money"]=$sxhhr1_buy_money;
			$rows["sxhhr1_buy_pay"]=$sxhhr1_buy_pay;
			$rows["sxhhr2_buy_money"]=$sxhhr2_buy_money;
			$rows["sxhhr2_buy_pay"]=$sxhhr2_buy_pay;
			$rows["sxhhr3_buy_money"]=$sxhhr3_buy_money;
			$rows["sxhhr3_buy_pay"]=$sxhhr3_buy_pay;
			$rows["hy1_buy_money"]=$hy1_buy_money;
			$rows["hy2_buy_money"]=$hy2_buy_money;
			$rows["hy3_buy_money"]=$hy3_buy_money;
			$rows["hy1_buy_pay"]=$hy1_buy_pay;
			$rows["hy2_buy_pay"]=$hy2_buy_pay;
			$rows["hy3_buy_pay"]=$hy3_buy_pay;
			$rows["hy1_tra_money"]=$hy1_tra_money;
			$rows["hy2_tra_money"]=$hy2_tra_money;
			$rows["hy3_tra_money"]=$hy3_tra_money;
			$rows["hy1_tra_pay"]=$hy1_tra_pay;
			$rows["hy2_tra_pay"]=$hy2_tra_pay;
			$rows["hy3_tra_pay"]=$hy3_tra_pay;
			$rows["hhr_hytd_buy"]=$hhr_hytd_buy;
			$rows["hhr_hytd_tra"]=$hhr_hytd_tra;
			$rows["hhr_hytd_zhf"]=$hhr_hytd_zhf;
			$rows["my_ly_jf"]=$my_ly_jf;
			$rows["my_buy_jf"]=$my_buy_jf;
			$this->assign('row',$rows);
			$this->assign('grade',$grade);
			//dump($rows);exit;
			$this->display();
	}
	public function tuiguang(){
		$this->check();
		$member=D('Myzzy');
		$id=session('id');
		$myMessage=$member->where(array('id'=>$id))->find();
		$myurl="http://" . $_SERVER['HTTP_HOST'] . "/index.php/Mobile/Myzzy/web/id/" . $id;
		$this->assign('myMessage',$myMessage);
		$this->assign('myurl',$myurl);
		$this->assign('id',$id);
		//生成二维码
		if(!file_exists("./Public/code/".$id.".png")) {

			$content = "http://" . $_SERVER['HTTP_HOST'] . "/index.php/Mobile/Myzzy/web/id/" . $id;
			vendor("phpqrcode.phpqrcode");
			$QRcode = new \QRcode();
			$QRcode::png($content, "./Public/code/" . $id . ".png", '', 6);
		}
		$this->display();
	}
	public function jlcx(){
		$this->check();
		$id=session('id');
		$grade=M('member','vip_')->where(array('id'=>$id))->getField('grade_name');
		$year=isset($_REQUEST['year'])?$_REQUEST['year']:date('Y',mktime());
		$month=isset($_REQUEST['month'])?$_REQUEST['month']:date('m',mktime());
		$end_year=$year+1;
		$person_month=D('person');
		$row=$person_month->where(array('month'=>$month,'year'=>$year,'member_id'=>$id))->find();
		$this->assign('year',$year);
		$this->assign('month',$month);
		$this->assign('row',$row);
		$this->assign('end_year',$end_year);
		$this->assign('grade',$grade);
		$this->display();
	}
/**
     * 判断两个id之间是否存在合伙人
     */
    public function vip2vip($id1,$id2){
        if($id1>$id2){
            $temp=$id1;
            $id1=$id2;
            $id2=$temp;
        }
        $m=M('member','vip_');
        $max=$m->where(array('id'=>$id2))->find();
        $arr=explode("-",$max['path']);
        $arr=array_reverse($arr);
        array_pop($arr);
        array_pop($arr);
        foreach($arr as $k=>$v){
            if($id1==$v){
                return true;
            }
            if($m->where(array('id'=>$v))->getField('grade_name')=='合伙人'){
                return false;
            }
        }
        return true;
    }

	//团队会员详情
	public function my_TD($return=0){
		$this->check();
		if(I('get.id')==''){
			$id=session('id');
		}else{
			$id=I('get.id');
		}
		if($id){
			//查询下级所有会员
			$vip=array();
			$my=$this->membermodel->where(array('id'=>$id))->find();
			$huiyuan=$this->membermodel->where(array('id'=>array('neq',$my['id']),'path'=>array('like','%'.$my['id'].'%'),'grade_name'=>'会员'))->select();
			foreach($huiyuan as $v=>$k){
				$arr=explode("-",$k['path']);
				$start=array_search($my['id'],$arr);
				$end=array_search($k['id'],$arr);
				if($end-$start==1 && $this->vip2vip($my['id'],$k['id'])){
					$vip[1][]=$k['id'];
				}elseif($end-$start==2 && $this->vip2vip($my['id'],$k['id'])){
					$vip[2][]=$k['id'];
				}elseif($end-$start==3 && $this->vip2vip($my['id'],$k['id'])){
					$vip[3][]=$k['id'];
				}elseif($end-$start>=4 && $this->vip2vip($my['id'],$k['id'])){
					$vip[4][]=$k['id'];
				}
			}
			if(count($vip[1])){
				$vip1=$this->membermodel->where(array('id'=>array('in',$vip[1])))->select();
			}else{
				$vip1=array();
			}
			//dump($vip1);
			//exit;
			if(count($vip[2])){
				$vip2=$this->membermodel->where(array('id'=>array('in',$vip[2])))->select();
			}else{
				$vip2=array();
			}
			if(count($vip[3])){
				$vip3=$this->membermodel->where(array('id'=>array('in',$vip[3])))->select();
			}else{
				$vip3=array();
			}
			//事业合伙人总人数
			$data=$this->membermodel->where(array('id'=>$id))->find();//根据ID查询出用户的所有信息
			$wheres['path']=array('like',$data['path'].'%');//得到用户的路径
			$rows=$this->membermodel->where($wheres)->select();//查询出所有下家
			$myname=$this->membermodel->where(array('id'=>$id))->getField('true_name');
			$zonghehuoren=0;      //下级合伙人总人数
			foreach($rows as $row){
				if($row['path']!=$data['path'] && $row['grade_name']=='合伙人'){
					++$zonghehuoren;
				}
			}
			foreach($rows as &$result){
				$result['path']=str_ireplace($data['path'],'',$result['path']);     //将下家所有的path字段中本身的path进行替换
				if(substr_count( $result['path'],'-')!=0 && $result['grade_name']=='合伙人'){
					if(substr_count( $result['path'],'-')==1){
						$firsts[]=$result;       //下一级所有合伙人
					}elseif(substr_count( $result['path'],'-')==2){
						$seconds[]=$result;      //下2级所有合伙人
					}elseif(substr_count( $result['path'],'-')==3){
						$thirds[]=$result;        //下三级所有合伙人
					}else{
						$others[]=$result;
					}
				}
			}
			foreach($firsts as &$first){
				$first['hehuoren']=$this->jisuan($first['id']);
			}
			foreach($seconds as &$first){
				$second['hehuoren']=$this->jisuan($first['id']);
			}
			foreach($thirds as &$first){
				$third['hehuoren']=$this->jisuan($first['id']);
			}
			if($return ==1 ){
				return array('td1'=>count($firsts)+count($vip1),'td2'=>count($firsts)+count($vip2),'td3'=>count($thirds)+count($vip3),);
			}
			$this->assign('data',$data);            //用户Id
			$this->assign('zonghehuoren',$zonghehuoren);            //合伙人总人数
			$this->assign('renshu_1',count($firsts));                  //一级合伙人人数
			$this->assign('renshu_2',count($seconds));
			$this->assign('renshu_3',count($thirds));
			$this->assign('firsts',$firsts);                            //一级合伙人信息
			$this->assign('seconds',$seconds);
			$this->assign('thirds',$thirds);
			$this->assign('vip1',$vip1);
			$this->assign('vip2',$vip2);
			$this->assign('vip3',$vip3);
			$this->assign('myname',$myname);
			
			//dump($seconds);
			//我的推荐人
			$tuijianren=$this->membermodel->where(array('id'=>$data['pid']))->find();   //推荐人相关系信息

			$this->assign('tuijianren',$tuijianren);
		}else{
			$this->error('Sorry,没有查询到相关信息');
		}
		$this->assign('grade',$my['grade_name']);

		$this->display();
	}
	public function getVipNum($id,$vip='合伙人'){
		$num=D('Myzzy')->where(array('grade_name'=>$vip,'path'=>array('like','%-'.$id.'-%')))->count();
		$num-=1;
		if($num<0){
			$num=0;
		}
		return $num;
	}
	public function tuan_zan($id=null){
		$this->check();
		if($id){
			$data=$this->membermodel->where(array('id'=>$id))->find();
			$wheres['path']=array('like',$data['path'].'%');
			$rows=$this->membermodel->where($wheres)->select();
			$zonghehuoren=0;      //下级合伙人总人数
			$zonghuiyuan=0;
			foreach($rows as $row){
				if($row['path']!=$data['path']&&$row['grade_name']!='会员'){
					++$zonghehuoren;
				}else{
					++$zonghuiyuan;
				}
			}
			foreach($rows as &$result){
				$result['path']=str_ireplace($data['path'],'',$result['path']);     //将下家所有的path字段中本身的path进行替换
				if(substr_count( $result['path'],'-')!=0&&$result['grade_name']!='会员'){
					if(substr_count( $result['path'],'-')==1){
						$firsts[]=$result;       //下一级所有合伙人
					}elseif(substr_count( $result['path'],'-')==2){
						$seconds[]=$result;      //下2级所有合伙人
					}elseif(substr_count( $result['path'],'-')==3){
						$thirds[]=$result;        //下三级所有合伙人
					}else{
						$others[]=$result;
					}
				}
			}
		}
		$data['hehuoren']=$this->jisuan($data['id']);
		$this->assign('data',$data);
		$this->assign('zonghehuoren',$zonghehuoren);
		$this->assign('zonghuiyuan',$zonghuiyuan);
		$this->assign('hehuoren1',count($firsts));
		$this->assign('hehuoren2',count($seconds));
		$this->assign('hehuoren3',count($thirds));
		$this->assign('other',count($others));
		$this->display();
	}
	//计算下家的人数
	public function jisuan($id,$type=1){
		$data=$this->membermodel->where(array('id'=>$id))->find();
		$wheres['path']=array('like',$data['path'].'%');
		$rows=$this->membermodel->where($wheres)->select();
		$sVipNum=$vipNum=$visitor=0;      //下级合伙人总人数
		foreach($rows as $row){
			if($row['path']!=$data['path'] && $row['grade_name']=='合伙人'){
				++$sVipNum;
			}
			if($row['path']!=$data['path'] && $row['grade_name']=='会员'){
				++$vipNum;
			}
			if($row['path']!=$data['path'] && $row['grade_name']=='游客'){
				++$visitor;
			}
		}
		if($type==0){
			return $visitor;
		}elseif($type==1){
			return $sVipNum;
		}else{
			return $vipNum;
		}

	}
	public function web(){
		session(null);
		cookie("mobile",null);
		cookie("phone",null);
		cookie("userid",null);
		cookie("user_id",null);
		$arr=array();
		preg_match('/[i]d[\/|=]\d{1,8}/',__SELF__,$arr);
		$id= substr($arr[0],-8,8);
		cookie('mytuijianren',$id,86400);
		//dump(cookie());exit;
		$this->display('erWeiMa');
	}
}