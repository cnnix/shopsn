<?php

namespace Mobile\Model;

use Think\Model;


/**
 * Created by PhpStorm.
 * @author 王强
 * @version 1.2
 */
class UserModel extends Model
{
    private $checkKey = array('username', 'password');
    
    private static $obj ;
   
    public function __construct(array $checKey = null)
    {
        parent::__construct();
        
        $this->checkKey = empty($checKey) ? $this->checkKey : $checKey;
    }
    /**
     * 登录 
     */
    public function login(array $data)
    {
        if (empty($data))
        {
            return array();
        }
      
        foreach ($this->checkKey as $name)
        {
            if (!array_key_exists($name, $data))
            {
                return array();
            }
        }
       
        $isSuccess = $this-> field($this->trueTableName.'.username,'.$this->trueTableName.'.id')
             -> join('inner join  vip_member as b ON '.$this->trueTableName.'.id = b.user_id')
             -> where($this->trueTableName.'.mobile ="'. $data['username'].'" AND '.$this->trueTableName.'.password = "'.$data['password'].'"')
             -> find();
        if (empty($isSuccess))
        {
            return false;
        }
        $_SESSION['userId'] = $isSuccess['id'];
        $isSuccess['token'] = session_id();
        unset($isSuccess['id']);
        return !empty($isSuccess)  ? $isSuccess :$_SESSION['userId']; 
    }
    
    public static function getInistnation(array $data = NULL)
    {
        $class = __CLASS__;
        return self::$obj = !(self::$obj instanceof $class) ? new self($data) : self::$obj;
    }
    
    /**
     * 检查该手机号码是否注册过 
     */
    public function isRegister($mobile)
    {
        return $this->where('mobile = "'.$mobile.'"')->count($this->getPk()) ? true : false;
    }
    
    public function add($data = '', $options = array(), $replace = false)
    {
        if (empty($data) || !is_array($data))
        {
            return false;
        }
        $data = $this->create($data);
        unset($data['id']);
        return parent::add($data, $options, $replace);
    }
    
    protected function _before_insert(& $data, $options)
    {
        $data['create_time'] = time();
        $data['status']      = 1;
        return $data;
    }
    
    /**
     * 获取用户信息 
     */
    public function getUserInfoById($field = 'mobile,integral', $default = 'find')
    {
        return $this->field($field)->where('id = "%s"', $_SESSION['userId'])->$default();
    }
    protected function _before_update( &$data, $options)
    {
        $data['update_time'] = time();
        return $data;
    }
     
    /**
     * {@inheritDoc}
     * @see \Think\Model::save()
     */
    
    public function save($data='', $options=array())
    {
        if (empty($data))
        {
            return false;
        }
        $data = $this->create($data);
    
        return parent::save($data, $options);
    }
    
    /**
     * 增加数据 
     */
    public function addUserData($integral)
    {
        if (empty($integral) || !is_numeric($integral))
        {
            return false;
        }
       
        $data = $this->getUserInfoById('integral,add_jf_currency,add_jf_limit');
        
        if (empty($data))
        {
            return $data;
        }
        
        foreach ($data as $key => &$value)
        {
            $value += $integral;
        }
        
        return $this->save($data, array('where' => array('id' => $_SESSION['userId'])));
    }
    
    /**
     * 是否存在该用户
     */
    public function isHaveUserByUserName($userName)
    {
        if (empty($userName))
        {
            return null;
        }
        return $this->where('mobile = "%s"', $userName)->getField($this->getPk());
    }
}