<?php
namespace Home\Controller;

use Common\Tool\Tool;

//前台模块
class PublicController extends BaseController
{
	//注册
	public function reg()
	{
	    $isOpen = $this->getConfig('sms_open');
	    //获取配置短信
		if(!empty($_POST)){
		    //
		   
		    if ($isOpen == 0 && $isOpen !== null && S('sms_code') != $_POST['sms_code'])
		    {
		        $this->error('手机验证码不正确');
		    }
			$m = M('user');
			$where['mobile'] = $_POST['mobile'];
			$result = $m->where($where)->find();
			if(!empty($result)){
				$this->error('手机号已注册，请更换');
			}
			$m = M('user');
			$where_m['id'] = isset($_POST['tuijianren_id']) ? $_POST['tuijianren_id'] : 0;
			$res_m = $m->where($where_m)->find();
// 			if(empty($res_m)){
// 				$this->error('推荐人ID错误,请重试');
// 			}

			$_POST['create_time'] = time();
			$_POST['password'] = md5($_POST['password']);
			$_POST['username'] = $_POST['mobile'];
			$res_id = $m->add($_POST);
			if($res_id){
                // 会员端用户数据同步操作
                $member = M('member','vip_');
                $_POST['pid'] = $_POST['tuijianren_id'];	//推荐人ID
                $where2['id'] = $_POST['pid'];
                $result = $member->field('path')->where($where2)->find();
                
                $_POST['path'] = $result['path'].$res_id.'-';	//当前path
                $_POST['id']   = $res_id;		//主键ID
                $_POST['grade_name'] = '会员';	//会员等级
                $_POST['user_id'] = $res_id;
                $_POST['status'] = 1;	//状态
                
                $_POST['true_name'] = $_POST['realname'];
                $_POST['card_id'] = $_POST['idcard'];
                
                $member->add($_POST);
				$_SESSION['user_id'] = $res_id;
				$_SESSION['mobile'] = $_POST['mobile'];
				$this->success('恭喜您，注册成功',U('User/user_info'));
			}else{
				$this->error('系统异常，注册失败');
			}
		}else{
		    $this->sms_open = $isOpen;
			$this->display();
		}
	}	
	
	//登陆
	public function login()
	{
		if(!empty($_POST)){			
			$m = M('user');
			$where['mobile'] = $_POST['mobile'];
			$mobile=str_replace(' ','',$_POST['mobile']);
			if(substr($mobile,1,1)==9){
				$this->redirect("Mobile/Other/index",array('account'=>$mobile,'pwd'=>$_POST['password']));
			}
			$result = $m->where($where)->find();
			if( empty($result)) {
				$this->error('没有该用户');
			} else {
				if (md5($_POST['password']) == $result['password']) {
					$_SESSION['user_id'] = $result['id'];
					$_SESSION['mobile'] = $_POST['mobile'];
					session('id',$result['id']);
					session('user_name', $result['username']);
							//查询是否是用户身份代码
            		$member = M('member','vip_');
            		$where_m['id'] = $_SESSION['user_id'];
            		$res_member = $member->where($where_m)->find();


//             		if($res_member['grade_name'] == '会员' || $res_member['grade_name'] == '合伙人'){
            			$this->success('登陆成功',U('User/user_info'));
//             		}else{
//             			$this->success('登陆成功',U('User/huiyuan_zige'));
//             		}
            	} else {
        			$this->error('密码不正确');
            	}				
	      }			
		}else{
			$this->display();
		}				
	}
	
	//发送短信
	public function ajax_send_mobile(){
		$res = self::send_sms($_GET['mobile']);
		if($res){
			$this->ajaxReturn($res);
		}else{
			$this->ajaxReturn(0);
		}
	}
	
	/**
	 * 短信验证函数封装
	 */
	public static function send_sms($mobile)
	{
	    //短信验证码
		header("Content-Type: text/html; charset=UTF-8");
		
		//生成随机数
		Tool::connect('PassMiMi');
		
		$verfity = Tool::getSmsCode();
		//获取短信配置
		$data = parent::getConfig();
		
		//以下信息自己填以下
		$argv = array(
				'name'=> $data['account'],     //必填参数。用户账号
				'pwd'=>  $data['sms_pwd'],     //必填参数。（web平台：基本资料中的接口密码）
				'content'=>str_replace('[xxx]', $verfity, $data['sms_content']),  //必填参数。发送内容（1-500 个汉字）UTF-8编码
				'mobile'=>$mobile,   //必填参数。手机号码。多个以英文逗号隔开
				'stime'=>'',   //可选参数。发送时间，填写时已填写的时间发送，不填时为当前时间发送
				'sign'=>'【'.mb_substr( strrchr($data['sms_content'], '，'), 1, 4, 'UTF-8').'】',    //必填参数。用户签名。
				'extno'=>''    //可选参数，扩展码，用户定义扩展码，只能为数字
		);
		$flag = 0;
		foreach ($argv as $key=>$value) {
			if ($flag != 0) {
				$params .= "&";
				$flag = 1;
			}
			$params.= $key."="; $params.= urlencode($value);// urlencode($value);
			$flag = 1;
		}
		$url = $data['sms_intnet'].'?'.$params; //提交的url地址
		//连接短信工具
		Tool::connect('Mosaic');
		$smsInformation = Tool::requestSms($url, array('type' => 'pt'));
		
		if ($smsInformation)
		{
		    S('sms_code',  $verfity, 45);
		}
		return $smsInformation;
	}
	
	//用户协议
	public function user_xieyi(){
		$this->display();
	}
	
	//忘记密码
	public function forget_pwd(){
		$this->display();
	}
	
	//重置密码
	public function update_pwd()
	{
		$info = M('User');
		$mobile = $_POST['mobile'];
		$data['password'] = md5($_POST['password']);
		$result = $info->where(array('mobile'=>$mobile))->save($data);
		if($result){
			$this->success('修改成功，请登录',U('Public/login'));
		}else{
			$this->success('修改失败，请重试');
		}
	}
    
	
}



