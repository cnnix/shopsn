<?php
namespace Admin\Model;

use Think\Model;

/**
 * 管理员模型 
 */
class AdminModel extends Model
{
    private static  $obj;
   
    public static function getInitnation()
    {
        $name = __CLASS__;
        return self::$obj = !(self::$obj instanceof $name) ? new self() : self::$obj;
    }
    
    /**
     * 获取登录的管理员信息 
     */
    public function getLoginUserInfo( $where, $field = 'id', $default='find')
    {
        if (empty($where))
        {
            return array();
        }
        return $this->field($field)->where($where)->$default();
    }
    
    /**
     * 重写添加操作
     */
    public function add($data = '', $options = array(),  $replace = false)
    {
        if (empty($data) || !is_array($data))
        {
            return array();
        }
         
        $addData  = $this->create($data);
        return parent::add($addData, $options, $replace);
    }
    
    /**
     * 添加前操作
     */
    protected function _before_insert(&$data, $options)
    {
        $data['create_time'] = time();
        $data['status']      = 1;
        $data['login_count'] = 0;
        return $data;
    }
    
    /**
     * 是否存在该用户 
     */
    public function isHaveUser($account)
    {
        if (empty($account))
        {
            return false;
        }
        $user = $this->getLoginUserInfo(array('account' => $account));
        return !empty($user) ? true : false;
    }
    
    /**
     * {@inheritDoc}
     * @see \Think\Model::save()
     */
    public function save($data='', $options=array())
    {
        if (empty($data))
        {
            return false;
        }
        $data = $this->create($data);
    
        return parent::save($data, $options);
    }
}