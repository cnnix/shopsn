<?php
namespace Admin\Model;

use Think\Model;
use Common\Tool\Tool;
use Common\TraitClass\callBackClass;

class GoodsModel extends Model
{
    use callBackClass;
    private static $obj ;
    
    public static function getInitation()
    {
        $class = __CLASS__;
        return self::$obj = !(self::$obj instanceof $class) ? new self() : self::$obj;
    }
    
    /**
     * 根据订单信息 查询商品数据 
     */
    public function getOrderInfo(array $data, $field, $fiter = FALSE)
    {
        if (empty($data) || empty($field)) {
            return array();
        }
        usort($data, array($this, 'compare'));
        $id = Tool::characterJoin($data);
        
        if (empty($id)) {
            return array();
        }
        
        $dataArray = $this->field($field,$fiter)->where('id in ('.$id.')')->select();
        
        if (empty($data)) {
            return array();
        }
        usort($dataArray, array($this, 'compare'));
        return $data = Tool::parseTwoArray($data, $dataArray);
        
    }
    
}