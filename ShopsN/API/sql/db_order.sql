/*
Navicat MySQL Data Transfer

Source Server         : yisu
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : zhang618

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-11-15 15:20:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `db_order`
-- ----------------------------
DROP TABLE IF EXISTS `db_order`;
CREATE TABLE `db_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_sn_id` varchar(50) DEFAULT '000000' COMMENT '订单标识',
  `price_sum` float(11,2) DEFAULT '0.00' COMMENT '总价',
  `express_id` int(11) DEFAULT '0',
  `address_id` int(11) DEFAULT NULL COMMENT '收货地址编号',
  `user_id` int(11) DEFAULT NULL COMMENT '用户编号',
  `create_time` varchar(50) DEFAULT NULL,
  `update_time` varchar(52) DEFAULT NULL,
  `order_status` enum('8','7','6','5','4','3','2','1','9','0') DEFAULT NULL COMMENT '0 未支付，1已支付，2，发货中，3已发货，4已收货，5退货,6退货审核中，7审核失败，8审核成功，退款中，9退款成功',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of db_order
-- ----------------------------
INSERT INTO `db_order` VALUES ('8', '201611111405414670532243', '0.00', '0', '1', '10001037', '1478844341', '1478844341', '0');
INSERT INTO `db_order` VALUES ('9', '201611111413485666168226', '0.00', '0', '1', '10001037', '1478844828', '1478844828', '0');
INSERT INTO `db_order` VALUES ('11', '201611111429415544769224', '1586.00', '0', '1', '10001037', '1478845781', '1478845781', '0');
INSERT INTO `db_order` VALUES ('12', '201611111431077723632833', '16000.00', '0', '1', '10001037', '1478845867', '1478845867', '0');
INSERT INTO `db_order` VALUES ('13', '201611111431503766632040', '99000.00', '0', '1', '10001037', '1478845910', '1478845910', '0');
INSERT INTO `db_order` VALUES ('14', '201611111434492062927232', '500.00', '0', '1', '10001037', '1478846089', '1478846089', '0');
INSERT INTO `db_order` VALUES ('15', '201611111440065780975328', '800.00', '0', '1', '10001037', '1478846406', '1478846406', '0');
INSERT INTO `db_order` VALUES ('16', '201611111441497032592729', '800.00', '0', '1', '10001037', '1478846509', '1478846509', '0');
INSERT INTO `db_order` VALUES ('17', '201611111443477755767812', '800.00', '0', '1', '10001037', '1478846627', '1478846627', '0');
INSERT INTO `db_order` VALUES ('18', '201611111446149941223136', '800.00', '0', '1', '10001037', '1478846774', '1478846774', '0');
INSERT INTO `db_order` VALUES ('19', '201611111447073327728230', '800.00', '0', '1', '10001037', '1478846827', '1478846827', '0');
INSERT INTO `db_order` VALUES ('20', '201611111450361191162146', '800.00', '0', '1', '10001037', '1478847036', '1478847036', '0');
INSERT INTO `db_order` VALUES ('21', '201611111452198352325433', '800.00', '0', '1', '10001037', '1478847139', '1478847139', '0');
INSERT INTO `db_order` VALUES ('22', '201611111453588028503431', '800.00', '0', '1', '10001037', '1478847238', '1478847238', '0');
INSERT INTO `db_order` VALUES ('23', '201611111454254653778026', '800.00', '0', '1', '10001037', '1478847265', '1478847265', '0');
INSERT INTO `db_order` VALUES ('24', '201611111457112293090835', '800.00', '0', '1', '10001037', '1478847431', '1478847431', '0');
INSERT INTO `db_order` VALUES ('25', '201611111459469775054912', '800.00', '0', '1', '10001037', '1478847586', '1478847586', '0');
INSERT INTO `db_order` VALUES ('26', '201611111503066168518037', '800.00', '0', '1', '10001037', '1478847786', '1478847786', '0');
