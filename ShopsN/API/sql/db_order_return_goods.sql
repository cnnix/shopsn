/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : yisushop

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-11-22 11:20:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `db_order_return_goods`
-- ----------------------------
DROP TABLE IF EXISTS `db_order_return_goods`;
CREATE TABLE `db_order_return_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `tuihuo_case` varchar(200) NOT NULL DEFAULT '就是退货' COMMENT '退货理由',
  `create_time` varchar(20) NOT NULL DEFAULT '0',
  `update_time` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of db_order_return_goods
-- ----------------------------
