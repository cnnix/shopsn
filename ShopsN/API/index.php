<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2014 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用入口文件
header("Content-type:text/html;charset=utf-8");
// 检测PHP环境
if(version_compare(PHP_VERSION,'5.4.0','<'))  die('require PHP > 5.4.0 !');

// 开启调试模式 建议开发阶段开启 部署阶段注释或者设为false
define('APP_DEBUG',true);

//默认分页长度
define('PAGE_SIZE', 20);

define('DEFAULT_MODULE', 'API');
// 定义应用目录
define('APP_PATH','./Application/');

defined('ACCESS') or define('ACCESS', './Application/Common/AlipayMobile/rsa_private_key.pem');
defined('PRV') or define('PRV','./Application/Common/AlipayMobile/rsa_public_key.pem');
if (strpos($_SERVER['PHP_SELF'], 'Admin') !== false)   
{
    echo file_get_contents('ErrorFiles/400.html');die();
}
// 引入ThinkPHP入口文件
require './Core/index.php';

// 亲^_^ 后面不需要任何代码了 就是如此简单