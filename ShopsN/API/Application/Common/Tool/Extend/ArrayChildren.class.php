<?php
namespace Common\Tool\Extend;
/**
 * 数组操作子类 
 */
class ArrayChildren extends ArrayParse
{
    /**
     * 计算和
     */
    public  function sumByArray(array $array, $sumKey = 'goods_price', $sumOne = 'goods_num', $sumSecond = 'fanli_jifen')
    {
        if (empty($array))
        {
            return $array;
        }
        $sum = 0;
        foreach ($array as $key => $value)
        {
            $sum += $value[$sumOne] * $value[$sumKey];
        }
        return $sum;
    }
    /**
     * 立即购买 
     */
    public  function buyNowMonery(array $array, $goods_num, $sumKey = 'goods_price', $sumSecond = 'fanli_jifen')
    {
        if (empty($array) || !is_numeric($goods_num))
        {
            return $array;
        }
        $sum = 0;
        foreach ($array as $key => $value)
        {
            $sum += $goods_num * $value[$sumKey];
        }
        return $sum;
    }
    /**
     * 添加数据
     */
    public function addValueAndKey(array $data, $setValue, $setDefault, $setKey = 'content')
    {
        if (empty($data))
        {
            return $data;
        }
        
        foreach ($data as $key => &$value)
        {
            $value[$setKey]       = $_POST[$setKey];
            $value['create_time']  = time();
            $value['user_id']     = $_SESSION['userId'];
            $value['status']      = $_POST['status'];
            $value['goods_orders_id'] = $_POST['goods_orders_id'];
        }
        
        return $data;
    }
    /**
     * 计算积分 
     */
    public function computationalIntegral(array $goods, $sumKey = 'fanli_jifen')
    {
        if (empty($goods))
        {
            return 0;
        }
        $sum = 0;
        
        foreach ($goods as $key => $value)
        {
            if (isset($value[$sumKey]))
            {
                $sum += $value[$sumKey];
            }
        }
        
        return $sum;
    }
    /**
     * 回归库存 
     */
    public function returnToStock(array $order, array $goods, $setKey = 'goods_num', $goodsKey = 'kucun')
    {
        if (empty($order) || empty($goods))
        {
            return false;
        }
        
        foreach ($goods as $key => &$value)
        {
            if ( $value['goods_id'] === $order[$key]['goods_id'])
            {
                $value[$goodsKey] += $order[$key][$setKey];
            }
        }
        return $goods;
    }
}