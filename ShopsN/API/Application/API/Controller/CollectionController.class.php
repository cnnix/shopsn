<?php
namespace API\Controller;

use API\Model\GoodsShoucangModel;
use Common\Tool\Tool;
use API\Model\GoodsModel;

/**
 * 收藏控制器 
 */
class CollectionController extends BaseController 
{
    public function __construct()
    {
        parent::__construct();
    
        $this->isLogin();
    }
    
    // 我的收藏
    public function personCollect()
    {
        
        $data = GoodsShoucangModel::getInitation()->getUserInfoById();
        
        $this->updateClient($data, '操作');
                
    }
    
    // 添加收藏
    public function  addCollect()
    {
        Tool::checkPost($_POST, array('is_numeric' => array('goods_id')), true , array('goods_id')) ? true : $this->ajaxReturnData(null, '400', '参数错误');
        
        // 判断是否是已经收藏了
        $isCollect = GoodsShoucangModel::getInitation()->isCollectionByUserId($_POST['goods_id']);
        $this->alreadyInData($isCollect, '已收藏该商品');
        
        $goodsData = GoodsModel::getInitation()->getGoodsById(array(
            'field' => 'title as detail_title,pic_url,price_old,price_new',
            'where' => array('id' => $_POST['goods_id'])
        ));
        
        $this->prompt($goodsData, 'detail_title', '找不到该商品');
        
        //连接工具
        Tool::connect('Mosaic');
        
        $goodsData = Tool::addValue($_POST, $goodsData);
        
        //$sql = "INSERT INTO db_goods_shoucang VALUES (null,'$goods_id','$user_id','$create_time','$pic_url','$price_old','$detail_title','$price_new','$is_type')";
        
        $status = GoodsShoucangModel::getInitation()->add($goodsData);
        
        $this->updateClient($status, '添加', true);
    }
    
    // 我收藏的删除
    public function  delCollect()
    {
        Tool::checkPost($_POST, array('is_numeric' => array('id')), true , array('id')) ? true : $this->ajaxReturnData(null, '400', '参数错误');
        
        
        $status = GoodsShoucangModel::getInitation()->delete( $_POST['id'] );
        
        $this->updateClient($status, '删除', true);
        
    }
    
    // 清空收藏
    public function emptyCollect()
    {
        $status = GoodsShoucangModel::getInitation()->delete( $_SESSION['userId'], 'user_id' );
        
        $this->updateClient($status, '清空', true);
    }
    
}