<?php
namespace API\Model;


use Think\Model;

/**
 * 我的足迹模型
 * @author 王强 
 */
class FootPrintModel extends Model
{
    private static $obj ;
    
        
    public static function getInitation()
    {
        $class = __CLASS__;
        return self::$obj = !(self::$obj instanceof $class) ? new self() : self::$obj;
    }
    
    public function add($goods, $options = array(), $replace = false)
    {
        if (empty($goods))
        {
            return false;
        }
        
        if ($this->where('gid = "%s"', $goods['id'])->getField($this->getPk()))
        {
            return false;
        }
        
        $footPrint = array(
            'gid'        => $goods['id'],
            'uid'        => $_SESSION['userId'],
            'goods_pic'  => $goods['pic_url'],
            'goods_name' => $goods['title'],
            'goods_price'=> $goods['price_new'],
            'is_type'    => 1
        );
        $insertId = parent::add($footPrint);
        
        return  $insertId;
    }
    
    /**
     * 获取足迹信息
     */
    public function getUserInfoById($field = 'id,gid,goods_pic,goods_name,goods_price,is_type', $default = 'select')
    {
        return $this->field($field)->where('uid = "%s"', $_SESSION['userId'])->$default();
    }
    
    protected function _before_insert(&$data, $options)
    {
        $data['create_time'] = time();
        
        return $data;
    }
    
    /**
     * 重新删除
     */
    public function delete($id, $field = null)
    {
        if (empty($id) || !is_numeric($id))
        {
            return false;
        }
        $pk = $field === null ? $this->getPk() : $field;
    
        return parent::delete(array(
            'where' => array($pk => $id)
        ));
    }
}
